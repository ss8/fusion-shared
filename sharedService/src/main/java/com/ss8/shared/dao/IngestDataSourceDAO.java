package com.ss8.shared.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;

import com.ss8.metahub.shared.dto.DataSourceEventDTO;
import com.ss8.metahub.shared.dto.DataSourceTypeDTO;
import com.ss8.metahub.shared.dto.ImportStatus;
import com.ss8.metahub.shared.dto.ImportStatusDTO;
import com.ss8.metahub.shared.dto.IngestDataImportDTO;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;
import com.ss8.metahub.shared.util.GlobalConstants;

@Repository("ingestDataSourceDao")
public class IngestDataSourceDAO extends BaseJdbcDao<IngestDataSourceDTO> {
	private static final Logger logger = LoggerFactory
			.getLogger(IngestDataSourceDAO.class);
	
	@Value("${fusion.datawipe.retension.days:30}")
	private String daysOfRetension;
	
	@Value("${h2.db:false}")
	public boolean isH2Db = false;

	private static final String INSERT_QUERY = "insert into ingest_datasource (name, description, eventType, userName, createdDate, importColumnsToTypeMap, usStyleDateFormat, path, fileType, streamType, datasourceType) values"
			+ " (?, ? ,? ,? , ? , ? , ?, ?, ?, ?, ?)";
	private static final String DELETE_QUERY = "delete from ingest_datasource where id = ?";
	private static final String SELECT_QUERY = "select ingest_datasource.id  , ingest_datasource.name  ,ingest_datasource.description ,ingest_datasource.eventType ,ingest_datasource.userName "
			+ " ,ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat ,ingest_datasource.createdDate , ingest_datasource.fileType, ingest_datasource.streamType, ingest_datasource.datasourceType, ingest_datasource.path, sum(ingest_dataimport.rowcount) as rowcount, sum(ingest_dataimport.failedrowcount) as failedrowcount, count( ingest_dataimport.id)  as importCount from "
			+ " ingest_datasource  left outer join ingest_dataimport  on ingest_datasource.id = ingest_dataimport.datasource group by  ingest_datasource.id order by id desc";
	private static final String SELECT_QUERY_ID = "select ingest_datasource.id  , ingest_datasource.name  ,ingest_datasource.description ,ingest_datasource.eventType ,ingest_datasource.userName "
			+ " ,ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat ,ingest_datasource.createdDate , ingest_datasource.fileType, ingest_datasource.streamType , ingest_datasource.datasourceType, ingest_datasource.path, sum(ingest_dataimport.rowcount) as rowcount, sum(ingest_dataimport.failedrowcount) as failedrowcount, count( ingest_dataimport.id)  as importCount from "
			+ " ingest_datasource  left outer join ingest_dataimport  on ingest_datasource.id = ingest_dataimport.datasource where ingest_datasource.id = ?  group by  ingest_datasource.id order by id desc";

	private static final String INSERT_QUERY_IMPORT = "insert into ingest_dataimport (name, description, userName, import_status , file_location ,  datasource, created_date)  values (?, ? ,? , ? ,? ,? ,?)";

	private static final String SELECT_QUERY_IMPORT = "select ingest_dataimport.id , ingest_dataimport.name , ingest_dataimport.file_location ,ingest_dataimport.description ,"
			+ "ingest_dataimport.import_status ,ingest_dataimport.datasource  , ingest_dataimport.error, ingest_dataimport.userName , ingest_dataimport.created_date ,ingest_dataimport.updated_date  ,"
			+ " ingest_dataimport.rowcount, ingest_dataimport.failedrowcount, ingest_datasource.id ,ingest_datasource.name , ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat,"
			+ "ingest_datasource.description,ingest_datasource.eventType, ingest_datasource.userName ,ingest_datasource.createdDate, ingest_datasource.fileType, ingest_datasource.datasourceType" 
			+ " from  ingest_dataimport  join  ingest_datasource   where  ingest_dataimport.datasource  = ingest_datasource.id   order by ingest_dataimport.id desc ";

	private static final String SELECT_QUERY_IMPORT_ID = "select ingest_dataimport.id , ingest_dataimport.name , ingest_dataimport.file_location ,ingest_dataimport.description ,"
			+ "ingest_dataimport.import_status ,ingest_dataimport.datasource  ,ingest_dataimport.userName ,ingest_dataimport.error,  ingest_dataimport.created_date ,ingest_dataimport.updated_date  ,"
			+ " ingest_dataimport.rowcount, ingest_dataimport.failedrowcount, ingest_datasource.id ,ingest_datasource.name , ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat,"
			+ "ingest_datasource.description,ingest_datasource.eventType, ingest_datasource.userName ,ingest_datasource.createdDate, ingest_datasource.fileType, ingest_datasource.datasourceType"
			+ " from  ingest_dataimport  join  ingest_datasource   where  ingest_dataimport.datasource  = ingest_datasource.id   and  ingest_dataimport.id = ?  ";
	
	private static final String SELECT_COUNT_QUERY_FOR_SUCCESSFUL_IMPORT_DS_ID = "select count(*) from ingest_dataimport where ingest_dataimport.datasource = ? "
			+ "and ingest_dataimport.import_status='INGESTED'";
	
	private static final String SELECT_COUNT_QUERY_FOR_IMPORT_DS_ID = "select count(*) from ingest_dataimport where ingest_dataimport.datasource = ?";

	private static final String SELECT_QUERY_IMPORT_DATASOURCE_ID = "select ingest_dataimport.id , ingest_dataimport.name , ingest_dataimport.file_location ,ingest_dataimport.description ,"
			+ "ingest_dataimport.import_status ,ingest_dataimport.datasource  ,ingest_dataimport.userName , ingest_dataimport.error, ingest_dataimport.created_date ,ingest_dataimport.updated_date  , "
			+ "ingest_dataimport.rowcount, ingest_dataimport.failedrowcount, ingest_datasource.id ,ingest_datasource.name ,ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat,"
			+ "ingest_datasource.description,ingest_datasource.eventType, ingest_datasource.userName ,ingest_datasource.createdDate, ingest_datasource.fileType, ingest_datasource.datasourceType"
			+ " from  ingest_dataimport  join  ingest_datasource   where  ingest_dataimport.datasource  = ingest_datasource.id   and  ingest_dataimport.datasource = ?   order by ingest_dataimport.id desc ";

	private static final String DELETE_QUERY_IMPORT = "delete from ingest_dataimport where id = ?  and import_status <> 'INGEST_STARTED'";

	private static final String INSERT_QUERY_DATASOURCE_EVENTTYPE = "insert into datasource_event_types (event_type)  values (?)";

	private static final String SELECT_QUERY_DATASOURCE_EVENTTYPE = "select id, event_type from datasource_event_types order by id desc";
	
	private static final String SELECT_QUERY_DATASOURCE_TYPES = "select id, datasource_type from datasource_types order by id asc";
	
	private static final String SELECT_DATAIMPORT_FOR_DATAWIPE = "select id , datasource  FROM ingest_dataimport WHERE updated_date <= DATE_SUB(NOW() , INTERVAL # DAY ) order by updated_date ASC limit 1;";
	private static final String SELECT_DATAIMPORT_FOR_DATAWIPE_H2 = "select id , datasource  FROM ingest_dataimport  order by updated_date ASC limit 1;";
	
	private static final String SELECT_QUERY_DATESOURCE_PAGINANTION = "select ingest_datasource.id  , ingest_datasource.name  ,ingest_datasource.description ,ingest_datasource.eventType ,ingest_datasource.userName "
			+ " ,ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat ,ingest_datasource.createdDate, ingest_datasource.fileType, ingest_datasource.streamType, ingest_datasource.datasourceType, ingest_datasource.path, sum(ingest_dataimport.rowcount) as totalRecords , sum(ingest_dataimport.rowcount) as rowcount, sum(ingest_dataimport.failedrowcount) as failedrowcount, count( ingest_dataimport.id)  as importCount from "
			+ " ingest_datasource  left outer join ingest_dataimport  on ingest_datasource.id = ingest_dataimport.datasource group by  ingest_datasource.id ";
	
	private static final String SELECT_QUERY_IMPORT_PAGINATION = "select ingest_dataimport.id , ingest_dataimport.name , ingest_dataimport.file_location ,ingest_dataimport.description ,"
			+ "ingest_dataimport.import_status ,ingest_dataimport.datasource  , ingest_dataimport.error, ingest_dataimport.userName , ingest_dataimport.created_date ,ingest_dataimport.updated_date  ,"
			+ " ingest_dataimport.rowcount, ingest_dataimport.failedrowcount, ingest_datasource.id ,ingest_datasource.name , ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat,"
			+ "ingest_datasource.description,ingest_datasource.eventType, ingest_datasource.userName ,ingest_datasource.createdDate, ingest_datasource.fileType, ingest_datasource.streamType, ingest_datasource.datasourceType"
			+ " from  ingest_dataimport  join  ingest_datasource   where  ingest_dataimport.datasource  = ingest_datasource.id  ";
	
	private static final String SELECT_QUERY_IMPORT_DATASOURCE_ID_PAGINATION = "select ingest_dataimport.id , ingest_dataimport.name , ingest_dataimport.file_location ,ingest_dataimport.description ,"
			+ "ingest_dataimport.import_status ,ingest_dataimport.datasource  ,ingest_dataimport.userName , ingest_dataimport.error, ingest_dataimport.created_date ,ingest_dataimport.updated_date  , "
			+ "ingest_dataimport.rowcount, ingest_dataimport.failedrowcount, ingest_datasource.id ,ingest_datasource.name ,ingest_datasource.importColumnsToTypeMap, ingest_datasource.usStyleDateFormat,"
			+ "ingest_datasource.description,ingest_datasource.eventType, ingest_datasource.userName ,ingest_datasource.createdDate, ingest_datasource.fileType, ingest_datasource.streamType, ingest_datasource.datasourceType"
			+ " from  ingest_dataimport  join  ingest_datasource   where  ingest_dataimport.datasource  = ingest_datasource.id   and  ingest_dataimport.datasource = ?   ";
	
	private static final String SELECT_QUERY_IMPORT_STATUS = "select import_status from ingest_dataimport where id = ?";
	
	private static final String UPDATE_IMPORT_STATUS = "update ingest_dataimport set import_status = ?, updated_date = ? where id  = ?";
	
	private static final String UPDATE_IMPORT_STATUS_AND_COUNT = "update ingest_dataimport set import_status = ?, rowcount =  ?, failedrowcount =  ifnull(failedrowcount,0) + ?, updated_date = ? where id  = ?";

	private static final String UPDATE_FAILED_RECORD_COUNT = "update ingest_dataimport set import_status = ?, failedrowcount =  ifnull(failedrowcount,0) + ? , updated_date = ?,"
			+ "    error =  ?  where id  = ?";
	
	private static final String UPDATE_FAILED_RECORD_COUNT_H2 = "update ingest_dataimport set import_status = ?, failedrowcount =  ?, updated_date = ?,"
			+ "    error = ? where id  = ?";
	
	static TimeZone tz = TimeZone.getTimeZone("UTC");
	static DateFormat df = new SimpleDateFormat(GlobalConstants.ISO_UTC_FORMAT);

	public IngestDataSourceDTO addDataSource(IngestDataSourceDTO dto) throws Exception {
		Date now = new Date();
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try {
			jdbcTemplate.update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
					ps.setString(1, dto.getName().trim());
					ps.setString(2, dto.getDesc());
					ps.setString(3, dto.getEventType());
					ps.setString(4, dto.getUserName());
					ps.setTimestamp(5, new Timestamp(now.getTime()));
					ps.setString(6, dto.getImportColumnsToTypeMap());
					ps.setBoolean(7, dto.isUsStyleDateFormat());
					ps.setString(8,  dto.getDatasourceFolderName());
					ps.setString(9, dto.getFileType());
					ps.setString(10, dto.getStreamType());
					if (dto.getDatasourceType() != null && !dto.getDatasourceType().isEmpty()) {
						ps.setString(11, dto.getDatasourceType());
					}
					return ps;
				}
			}, keyHolder);
		} catch (DuplicateKeyException e) {
            throw new Exception("name Allready exists");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		dto.setId(keyHolder.getKey().intValue());
		df.setTimeZone(tz);
		dto.setCreatedDate(df.format(now));
		return dto;

	}

	public List<IngestDataSourceDTO> getDataSources() {
		return (this.jdbcTemplate.query(SELECT_QUERY, new DataSourceRowMapper()));

	}
	
	public List<IngestDataSourceDTO> getDataSources( String orderBy ,int startRow , int endRow , String col_name) {
		//TODO clean up the sql and align with UI instead of unwanted columns sent back to UI 
		Set<String>  derived_columns = new HashSet<>();
		derived_columns.add("importcount");
		derived_columns.add("rowcount");
		derived_columns.add("failedrowcount");
		derived_columns.add("totalrecords");
		String final_query_pagination = SELECT_QUERY_DATESOURCE_PAGINANTION +" order by " + ( !derived_columns.contains(col_name.toLowerCase()) ?  "ingest_datasource." :"" ) + orderBy + " limit " +startRow +" , " + endRow;
		return (this.jdbcTemplate.query(final_query_pagination, new DataSourceRowMapper()));

	}

	public List<IngestDataSourceDTO> getDataSourcebyId(int id) {
		return (this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(SELECT_QUERY_ID);
				ps.setInt(1, id);
				return ps;
			}
		}, new DataSourceRowMapper()));

	}

	public void deleteDataSource(int id) throws Exception {
		// Get the data source that needs to be deleted.
		List<IngestDataSourceDTO> datasourceDTOList = getDataSourcebyId(id);
		if (datasourceDTOList != null && datasourceDTOList.size() > 0) {
			int status = jdbcTemplate.update(DELETE_QUERY, id);
			if (status == 0) {
				throw new Exception("Id Not Found Or Unable to delete data for id " + id);
			}
			// Delete the data source directory.
			IngestDataSourceDTO datasourceDTO = datasourceDTOList.get(0);
			if (datasourceDTO.getDatasourceFolderName() != null) {
				File datasourceDirectory = new File(datasourceDTO.getDatasourceFolderName());
				if (datasourceDirectory.exists()) {
					boolean directoryDeletionStatus = FileSystemUtils.deleteRecursively(datasourceDirectory);
					if (!directoryDeletionStatus) {
						throw new Exception("Datasource directory - " + datasourceDTO.getDatasourceFolderName() + " was not deleted for datasource id - " + id);
					}
				}
			}
		} else {
			throw new Exception("Id Not Found Or Unable to delete data for id " + id);
		}	
	}

	private static final class DataSourceRowMapper implements RowMapper<IngestDataSourceDTO> {
		public IngestDataSourceDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			IngestDataSourceDTO dto = new IngestDataSourceDTO();
			dto.setId(rs.getInt("ingest_datasource.id"));
			dto.setName(rs.getString("ingest_datasource.name"));
			dto.setUserName(rs.getString("ingest_datasource.userName"));
			dto.setDesc(rs.getString("ingest_datasource.description"));
			dto.setEventType(rs.getString("ingest_datasource.eventType"));
			dto.setImportCount(rs.getLong("importCount"));
			dto.setRowCount(rs.getLong("rowcount"));
			dto.setFailedRowCount(rs.getLong("failedrowcount"));
			df.setTimeZone(tz);
			dto.setCreatedDate(df.format(rs.getTimestamp("ingest_datasource.createdDate")));
			dto.setImportColumnsToTypeMap(rs.getString("ingest_datasource.importColumnsToTypeMap"));
			dto.setUsStyleDateFormat(rs.getBoolean("ingest_datasource.usStyleDateFormat"));
			dto.setTotalRecords(rs.getLong("rowcount"));
			dto.setCanDelete(rs.getInt("importCount") == 0 ? true : false);
			dto.setFileType(rs.getString("ingest_datasource.fileType"));
			dto.setStreamType(rs.getString("ingest_datasource.streamType"));
			dto.setDatasourceFolderName(rs.getString("ingest_datasource.path"));
			dto.setDatasourceType(rs.getString("ingest_datasource.datasourceType"));
			return dto;
		}
	}
	
	private static final class DataImportRowMapperForDataWipe implements RowMapper<IngestDataImportDTO> {
		public IngestDataImportDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			IngestDataImportDTO dto = new IngestDataImportDTO();
            dto.setId(rs.getLong("id"));
			dto.setDatasource(rs.getInt("datasource"));
			return dto;
		}
	}

	private static final class DataImportRowMapper implements RowMapper<IngestDataImportDTO> {
		public IngestDataImportDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			IngestDataImportDTO dto = new IngestDataImportDTO();

			dto.setId(rs.getLong("ingest_dataimport.id"));
			dto.setFilename(rs.getString("ingest_dataimport.file_location"));
			dto.setStatus(rs.getString("ingest_dataimport.import_status"));
			dto.setName(rs.getString("ingest_dataimport.name"));
			dto.setDescription(rs.getString("ingest_dataimport.description"));
			dto.setDatasource(rs.getInt("ingest_dataimport.datasource"));
			dto.setUserName(rs.getString("ingest_dataimport.username"));
			dto.setError(rs.getString("ingest_dataimport.error"));
			dto.setRowCount(rs.getLong("ingest_dataimport.rowcount"));
			dto.setFailedRowCount(rs.getLong("ingest_dataimport.failedrowcount"));
			df.setTimeZone(tz);
			dto.setCreatedDate(df.format(rs.getTimestamp("ingest_dataimport.created_date")));
			if (null != rs.getTimestamp("ingest_dataimport.updated_date"))
				dto.setUpdatedDate(df.format(rs.getTimestamp("ingest_dataimport.updated_date")));
			IngestDataSourceDTO dataSourcedto = new IngestDataSourceDTO();
			dataSourcedto.setId(rs.getInt("ingest_datasource.id"));
			dataSourcedto.setName(rs.getString("ingest_datasource.name"));
			dataSourcedto.setUserName(rs.getString("ingest_datasource.userName"));
			dataSourcedto.setDesc(rs.getString("ingest_datasource.description"));
			dataSourcedto.setEventType(rs.getString("ingest_datasource.eventType"));
			dataSourcedto.setCreatedDate(df.format(rs.getTimestamp("ingest_datasource.createdDate")));
			dataSourcedto.setImportColumnsToTypeMap(rs.getString("ingest_datasource.importColumnsToTypeMap"));
			dataSourcedto.setUsStyleDateFormat(rs.getBoolean("ingest_datasource.usStyleDateFormat"));
			dataSourcedto.setFileType(rs.getString("ingest_datasource.fileType"));
			dataSourcedto.setDatasourceType(rs.getString("ingest_datasource.datasourceType"));
			dto.setDatasourceData(dataSourcedto);
			return dto;
		}
	}

	private static final class DataSourceEventTypeRowMapper implements RowMapper<DataSourceEventDTO> {
		public DataSourceEventDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			DataSourceEventDTO dto = new DataSourceEventDTO();
			dto.setId(rs.getInt("id"));
			dto.setEventType(rs.getString("event_type"));
			return dto;
		}
	}
	
	private static final class DataSourceTypeRowMapper implements RowMapper<DataSourceTypeDTO> {
		public DataSourceTypeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			DataSourceTypeDTO dto = new DataSourceTypeDTO();
			dto.setId(rs.getInt("id"));
			dto.setDatasourceType(rs.getString("datasource_type"));
			return dto;
		}
	}
	
	public List<IngestDataImportDTO> getDataImportForDataWipe() {

		String query = null;
		if (isH2Db)
			query = SELECT_DATAIMPORT_FOR_DATAWIPE_H2;
		else
			query = SELECT_DATAIMPORT_FOR_DATAWIPE;
		query = query.replace("#", daysOfRetension);
		List<IngestDataImportDTO> list = (this.jdbcTemplate.query(query, new DataImportRowMapperForDataWipe()));
		if (list == null || list.isEmpty())
			return null;
		return list;
	}

	public IngestDataImportDTO addDataImport(IngestDataImportDTO dto) {
		Date now = new Date();
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(INSERT_QUERY_IMPORT, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, dto.getName().trim());
				ps.setString(2, dto.getDescription());
				ps.setString(3, dto.getUserName());
				ps.setString(4, dto.getStatus());
				ps.setString(5, dto.getFilename());
				ps.setInt(6, dto.getDatasource());
				ps.setTimestamp(7, new Timestamp(now.getTime()));
				return ps;
			}
		}, keyHolder);

		dto.setId(keyHolder.getKey().longValue());
		df.setTimeZone(tz);
		dto.setCreatedDate(df.format(now));
		return dto;
	}

	public List<IngestDataImportDTO> getDataImports() {
		return (this.jdbcTemplate.query(SELECT_QUERY_IMPORT, new DataImportRowMapper()));
	}
	
	public List<IngestDataImportDTO> getDataImports( String orderBy ,int startRow , int endRow) {
		//TODO clean up the sql and align with UI instead of unwanted columns sent back to UI 
		String final_query_pagination = SELECT_QUERY_IMPORT_PAGINATION +" order by ingest_dataimport." + orderBy + " limit " +startRow +" , " + endRow;
		return (this.jdbcTemplate.query(final_query_pagination, new DataImportRowMapper()));
	}

	public List<IngestDataImportDTO> getDataImportbyId(int id) {
		if(logger.isDebugEnabled()) {
			logger.debug("getDataImportbyId: id - " + id);
		}
		
		return (this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				if (logger.isDebugEnabled()) {
					logger.debug("SELECT_QUERY_IMPORT_ID - "
							+ SELECT_QUERY_IMPORT_ID);
				}
				
				PreparedStatement ps = connection.prepareStatement(SELECT_QUERY_IMPORT_ID);
				ps.setInt(1, id);
				return ps;
			}
		}, new DataImportRowMapper()));
	}
	
	public int countSuccessfullyIngestedImportsByDsId(final int id) {
		return this.jdbcTemplate.queryForObject(SELECT_COUNT_QUERY_FOR_SUCCESSFUL_IMPORT_DS_ID, Integer.class, id);
	}
	
	public int countImportsByDsId(final int id) {
		return this.jdbcTemplate.queryForObject(SELECT_COUNT_QUERY_FOR_IMPORT_DS_ID, Integer.class, id);
	}

	public void deleteDataImport(int id) throws Exception {
		int status = jdbcTemplate.update(DELETE_QUERY_IMPORT, id);
		if (status == 0)
			throw new Exception("Id Not Found Or status ingest in progress. Unable to delete data for id " + id);

	}

	public List<IngestDataImportDTO> getDataImportbyDataSourceId(int id) {

		return (this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(SELECT_QUERY_IMPORT_DATASOURCE_ID);
				ps.setInt(1, id);
				return ps;
			}
		}, new DataImportRowMapper()));
	}
	
	public List<IngestDataImportDTO> getDataImportbyDataSourceId(int id ,String orderBy ,int startRow ,int endRow) {
		String final_query_pagination = SELECT_QUERY_IMPORT_DATASOURCE_ID_PAGINATION +" order by ingest_dataimport." + orderBy + " limit " +startRow +" , " + endRow;
		
		return (this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(final_query_pagination);
				ps.setInt(1, id);
				return ps;
			}
		}, new DataImportRowMapper()));
	}

	public DataSourceEventDTO addDataSourceEventType(DataSourceEventDTO dto) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(INSERT_QUERY_DATASOURCE_EVENTTYPE, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, dto.getEventType());
				return ps;
			}
		}, keyHolder);
		dto.setId(keyHolder.getKey().intValue());
		return dto;
	}
	
	public int getDataSourceEventTypeIdByEventType(final String eventType) {
		List<Integer> eventTypesIdList = this.jdbcTemplate.query(
				"select id from datasource_event_types where event_type='" + eventType + "'", new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt(1);
			}
		});
		if (eventTypesIdList.size() > 0) {
			return eventTypesIdList.get(0);
		} else {
			return 0;
		}
	}

	public List<DataSourceEventDTO> getDataSourceEventTypes() {
		return (this.jdbcTemplate.query(SELECT_QUERY_DATASOURCE_EVENTTYPE, new DataSourceEventTypeRowMapper()));
	}
	
	public List<DataSourceTypeDTO> getDataSourceTypes() {
		return (this.jdbcTemplate.query(SELECT_QUERY_DATASOURCE_TYPES, new DataSourceTypeRowMapper()));
	}
	
	public String getDataImportStatus(int id) throws Exception {
		return this.jdbcTemplate.queryForObject(SELECT_QUERY_IMPORT_STATUS, String.class, id);
	}
	
	public void updateFailedRecordsCount(long id, long failedRecords, final ImportStatus importStatus, final String errorMessage) throws Exception {
		int status = 0;
		if (isH2Db) {
			status = jdbcTemplate.update(UPDATE_FAILED_RECORD_COUNT_H2, importStatus.toString(), failedRecords, new Date(), errorMessage, id);
		} else {
			status = jdbcTemplate.update(UPDATE_FAILED_RECORD_COUNT, importStatus.toString(), failedRecords, new Date(), errorMessage, id);
		}
		if (status == 0) {
			throw new Exception("unable to update import records for ID: " + id);
		}
	}
	
	public void updateImportStatusWithRowCountAndFailedCount(long id, long rowCount, long failedRowCount, final ImportStatus importStatus) throws Exception {
		int status = 0;
		status = jdbcTemplate.update(UPDATE_IMPORT_STATUS_AND_COUNT, importStatus.toString(), rowCount, failedRowCount, new Date(), id);
		if (status == 0) {
			throw new Exception("unable to update import records for ID: " + id);
		}
	}
	
	public void updateImportStatusOnly(long id, final ImportStatus importStatus) throws Exception {
		int status = 0;
		status = jdbcTemplate.update(UPDATE_IMPORT_STATUS, importStatus.toString(), new Date(), id);
		if (status == 0) {
			throw new Exception("unable to update import records for ID: " + id);
		}
	}
	
	public void updateDataImportStatus(final long id, final ImportStatusDTO importStatusDTO) throws Exception {
		if (ImportStatus.INGEST_FAILED.equals(importStatusDTO.getStatus()) || ImportStatus.STREAMING_INGEST_FAILED.equals(importStatusDTO.getStatus())) {
			this.updateFailedRecordsCount(id, importStatusDTO.getFailedRecordCount(), importStatusDTO.getStatus(), importStatusDTO.getErrorMessage());
		} else if (ImportStatus.INGESTED.equals(importStatusDTO.getStatus()) || ImportStatus.STREAMING_INGESTED.equals(importStatusDTO.getStatus())) {
			this.updateImportStatusWithRowCountAndFailedCount(id, importStatusDTO.getTotalRecordCount(), importStatusDTO.getFailedRecordCount(), importStatusDTO.getStatus());
		} else {
			this.updateImportStatusOnly(id, importStatusDTO.getStatus());
		}
	}
}

