package com.ss8.shared.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.ss8.metahub.shared.dto.DocumentDTO;
import com.ss8.metahub.shared.dto.FieldDisplayName;
import com.ss8.shared.dao.BaseJdbcDao;
import com.ss8.shared.elastic.service.ElasticSearchService;

@Repository("sharedImportDao")
public class SharedImportDao extends BaseJdbcDao<DocumentDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SharedImportDao.class);

	private static final String SELECT_QUERY_FIELDS_TO_DISPLAY_NAME_MAPPING_QUERY = "select field, displayName, fieldType, collection from query_fields where collection in (%s)";

	private static final String SELECT_QUERY_FIELDS_QUERY_BY_COLLECTION = "select field from query_fields where collection = ?";
	
	private static final String INSERT_QUERY_FIELDS_MAPPING_QUERY_H2 = "insert into query_fields (field, displayName ,fieldType, collection) values (?, ? ,?, ?)";

	private static final String INSERT_QUERY_FIELDS_MAPPING_QUERY = "insert IGNORE into query_fields (field, displayName ,fieldType, collection) values (?, ? ,?, ?)";

	private RestHighLevelClient restHighLevelClient;
	
	@Value("${h2.db:false}")
	private boolean isH2;

	@Autowired
	private ElasticSearchService elasticCacheService;

	public SharedImportDao(@Qualifier("elasticSearchConfiguration") RestHighLevelClient restHighLevelClient) {
		this.restHighLevelClient = restHighLevelClient;
	}

	@PostConstruct
	public void init() {
	}

	public void updateImportFieldDisplayNameMappings(final FieldDisplayName field) {
		// Update the query fields mapping.
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(!isH2 ? INSERT_QUERY_FIELDS_MAPPING_QUERY : INSERT_QUERY_FIELDS_MAPPING_QUERY_H2);
				ps.setString(1, field.getField());
				ps.setString(2, field.getDisplayName());
				ps.setString(3, field.getDataType());
				ps.setString(4, field.getCollection());
				return ps;
			}
		});
	}

	public static final class FieldDisplayRowMapper implements RowMapper<FieldDisplayName> {
		public FieldDisplayName mapRow(ResultSet rs, int rowNum) throws SQLException {
			FieldDisplayName fieldDisplayName = new FieldDisplayName();
			fieldDisplayName.setField(rs.getString("field"));
			fieldDisplayName.setDisplayName(rs.getString("displayName"));
			fieldDisplayName.setDataType(rs.getString("fieldType"));
			fieldDisplayName.setCollection(rs.getString("collection"));
			return fieldDisplayName;
		}
	}

	public List<FieldDisplayName> getFieldDisplayNameMapping(final ArrayList<String> sources) {
		String inSql = String.join(",", Collections.nCopies(sources.size(), "?"));
		return this.jdbcTemplate.query(String.format(SELECT_QUERY_FIELDS_TO_DISPLAY_NAME_MAPPING_QUERY, inSql),
				new FieldDisplayRowMapper(), sources.toArray());
	}

	private static final class FieldRowMapper implements RowMapper<String> {
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getString("field");
		}
	}

	public TreeSet<String> getQueryFieldsByCollection(final String collection) {
		TreeSet<String> queryFields = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		queryFields.addAll(new HashSet<String>(
				this.jdbcTemplate.query(SELECT_QUERY_FIELDS_QUERY_BY_COLLECTION, new FieldRowMapper(), collection)));
		return queryFields;
	}

	public boolean deleteByImportAndDatasourceId(final long importId, final long datasourceId, final String readIndexName) {
		try {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("deleteByImportAndDatasourceId: importId - "
						+ importId + ", datasourceId - " + datasourceId
						+ ", readIndexName - " + readIndexName);
			}
			
			BoolQueryBuilder query = new BoolQueryBuilder().must(new TermQueryBuilder("importId", importId))
					.must(new TermQueryBuilder("datasourceId", datasourceId));
			String indices = elasticCacheService.getAliasIndices(readIndexName);
			DeleteByQueryRequest request = new DeleteByQueryRequest(indices);
			request.setQuery(query);
			request.setBatchSize(1000);
			BulkByScrollResponse response = restHighLevelClient.deleteByQuery(request, RequestOptions.DEFAULT);
			long deleted = response.getDeleted();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(
					"SharedImportDao.deleteImportByDatasourceId():: Exception while deleting import documents by import id and datasource id - "
							+ e.getMessage());
			return false;
		}
		return true;
	}

	public boolean foundCountByImportAndDatasourceId(final long importId, final long datasourceId, final String readIndexName) {
		try {
			BoolQueryBuilder query = new BoolQueryBuilder().must(new TermQueryBuilder("importId", importId))
					.must(new TermQueryBuilder("datasourceId", datasourceId));
			CountRequest countRequest = new CountRequest(readIndexName);
			countRequest.query(query);
			CountResponse countResponse = restHighLevelClient.count(countRequest, RequestOptions.DEFAULT);
			return countResponse.getCount() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(
					"SharedImportDao.foundCountByImportAndDatasourceId():: Exception while fetching count by import id and datasource id - "
							+ e.getMessage());
			return false;
		}
	}
	
	public class ImportException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ImportException(final String errorMessage) {
			super(errorMessage);
		}
	}
}
