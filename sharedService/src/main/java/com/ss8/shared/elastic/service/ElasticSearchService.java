package com.ss8.shared.elastic.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class ElasticSearchService {

    private RestHighLevelClient restHighLevelClient;

    public ElasticSearchService(@Qualifier("elasticSearchConfiguration") RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    public String getAliasIndices(String aliasName) throws Exception {
        List<Map<String, String>> aliasIndices = getAliasDetails(aliasName);

        String indices = aliasIndices.stream()
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .filter(entry -> "name".equals(entry.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.joining(","));

        return indices;
    }

    @Cacheable(value = "elasticAlias", key = "#aliasName")
    public List<Map<String, String>> getAliasDetails(String aliasName) throws Exception {

        List<Map<String, String>> indices = new ArrayList<>();

        GetIndexRequest request = new GetIndexRequest(aliasName);
        GetIndexResponse response = restHighLevelClient.indices().get(request, RequestOptions.DEFAULT);
        response.getIndices();

        for(String indexName : response.getIndices()) {
            Map<String, String> indexMap = new HashMap<>();
            indexMap.put("name", indexName);

            String creationDate = response.getSetting(indexName, "index.creation_date");
            indexMap.put("creation_date", creationDate);

            indices.add(indexMap);
        }
        if(indices.size() == 0) throw new Exception("No index attached to alias");

        return indices;
    }
}