package com.ss8.shared.service;

import java.util.List;

import com.ss8.metahub.shared.dto.ImportStatusDTO;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;

public interface DataIngestService {
	
	public void updateImportStatus(final String importStatusUrl, final long importId, final ImportStatusDTO importStatusDTO) throws Exception;
	
	public List<IngestDataSourceDTO> getAllDatasources(final String datasourcesUrl) throws Exception;
}
