package com.ss8.shared.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
	public String storeFile(final MultipartFile file, final String fileStoreLocation) throws Exception;
}
