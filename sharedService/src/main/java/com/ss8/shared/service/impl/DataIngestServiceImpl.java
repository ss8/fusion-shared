package com.ss8.shared.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.metahub.shared.dto.EventsResponseDto;
import com.ss8.metahub.shared.dto.GenericResponse;
import com.ss8.metahub.shared.dto.ImportStatusDTO;
import com.ss8.metahub.shared.dto.IngestDataSourceDTO;
import com.ss8.shared.service.DataIngestService;

@Service
public class DataIngestServiceImpl implements DataIngestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataIngestServiceImpl.class);
	
	@Lazy
	@Autowired
	private RestTemplate sharedRestTemplate;
	
	@Bean
	@Qualifier("sharedRestTemplate")
	public RestTemplate sharedRestTemplate() {
	    return new RestTemplate();
	}

	@Override
	public void updateImportStatus(final String importStatusUrl, final long importId, final ImportStatusDTO importStatusDTO) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			String writeValueAsString = objectMapper.writeValueAsString(importStatusDTO);
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(writeValueAsString, headers);
			ResponseEntity<GenericResponse> response = sharedRestTemplate.exchange(importStatusUrl, HttpMethod.POST,
					requestEntity, GenericResponse.class);
			if (response.getStatusCodeValue() != 200) {
				LOGGER.error("DataIngestServiceImpl :: updateImportStatus() - " + response.getBody());
				throw new Exception("Error while updating import status for import id - " + importId 
						+ ". The error is - " + response.getBody().getMessage() + " and the error code is - " + response.getBody().getCode());
			}
		} catch (Exception e) {
			LOGGER.error("DataIngestServiceImpl :: updateImportStatus() - Error while updating import status for import id - " 
					+ importId + " . The error message is - " + e.getMessage());
			throw new Exception("Error while updating import status for import id - " + importId + ". The error is - " + e.getMessage());
		}
	}

	@Override
	public List<IngestDataSourceDTO> getAllDatasources(final String datasourcesUrl) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<EventsResponseDto<IngestDataSourceDTO>> response = null;
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptableMediaTypes);
		headers.setContentType(MediaType.APPLICATION_JSON);
		try {
			HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);
			response = sharedRestTemplate.exchange(datasourcesUrl, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<EventsResponseDto<IngestDataSourceDTO>>() {});
			if (response.getStatusCodeValue() != 200) {
				LOGGER.error("DataIngestServiceImpl :: getAllDatasources() - " + response.getBody());
				throw new Exception("Error while fetching all datasources. The error is - " + response.getBody());
			}
			return response.getBody().getData();
		} catch (Exception e) {
			LOGGER.error("DataIngestServiceImpl :: getAllDatasources() - fetching all datasources. The error message is - " + e.getMessage());
			throw new Exception("Error while fetching all datasources. The error is - " + e.getMessage());
		}
	}
}
