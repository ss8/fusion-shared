package com.ss8.shared.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ss8.metahub.shared.util.FilesUtil;
import com.ss8.shared.service.FileStorageService;

@Service
public class FileStorageServiceImpl implements FileStorageService {

	@Override
	public String storeFile(final MultipartFile file, final String fileStoreDirectoryLocation) throws Exception {
		Path fileStorageLocation = Paths.get(fileStoreDirectoryLocation).toAbsolutePath().normalize();
		try {
			FilesUtil.getInstance().createDirectoriesWithDefaultAttributes(
					fileStorageLocation);
		} catch (Exception ex) {
			throw new Exception("Could not create the directory where the uploaded files will be stored.", ex);
		}
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename() + ".watched");

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains(".."))
				throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);

			// Copy file to the target location (Replacing existing file with
			// the same name)
			Path targetLocation = fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			return targetLocation.toString();
		} catch (Exception ex) {
			throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
		}
	}
}
