package com.ss8.metahub.shared.dto;

public class MetadataRecord {
	
	public enum MetadataFieldDataType {
		String, Date, Time, DateTime, Number, GeoPoint, Array
	}
	
	private String fieldName;
	
	private String dataType;
	
	private String mappedField;
	
	private boolean mandatory;
	
	private String fieldToMergeTo;
	
	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the mappedField
	 */
	public String getMappedField() {
		return mappedField;
	}

	/**
	 * @param mappedField the mappedField to set
	 */
	public void setMappedField(String mappedField) {
		this.mappedField = mappedField;
	}

	/**
	 * @return the mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * @param mandatory the mandatory to set
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * @return the fieldToMergeTo
	 */
	public String getFieldToMergeTo() {
		return fieldToMergeTo;
	}

	/**
	 * @param fieldToMergeTo the fieldToMergeTo to set
	 */
	public void setFieldToMergeTo(String fieldToMergeTo) {
		this.fieldToMergeTo = fieldToMergeTo;
	}
}
