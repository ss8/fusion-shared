package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ImportRecord {
	
	private long importId;
	
	private long dataSourceId;
	
	private String dataSourceName;
	
	private String importName;
	
	private String eventType;
	
	private boolean usStyleDateFormat = false;
	
	private String filePath;

	private String fileType;
	
	private String datasourceType;
	
	private String datasourceSubType;
	
	private HashMap<String, Object> metadata;
	
	private boolean allowZipFiles;

	private String unprocessedUrlPath;
	
	@JsonIgnore
	private String createdDate;
	
	@JsonIgnore
	private IngestDataSourceDTO datasourceData;

	@JsonIgnore
	private LinkedHashMap<String, ImportField> importColumnsToTypeMap = new LinkedHashMap<String, ImportField>();

    /**
	 * @return the importColumnsToTypeMap
	 */
	@JsonAnyGetter
	public LinkedHashMap<String, ImportField> getImportColumnsToTypeMap() {
		return importColumnsToTypeMap;
	}

	/**
	 * @param name,importField the importColumnsToTypeMap to set
	 */
	@JsonAnySetter
	public void setImportColumnsToTypeMap(String name, ImportField importField) {
		this.importColumnsToTypeMap.put(name, importField);
	}

	/**
	 * @return the importId
	 */
	public long getImportId() {
		return importId;
	}

	/**
	 * @param importId the importId to set
	 */
	public void setImportId(long importId) {
		this.importId = importId;
	}

	/**
	 * @return the dataSourceId
	 */
	public long getDataSourceId() {
		return dataSourceId;
	}

	/**
	 * @param dataSourceId the dataSourceId to set
	 */
	public void setDataSourceId(long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	/**
	 * @return the dataSourceName
	 */
	public String getDataSourceName() {
		return dataSourceName;
	}

	/**
	 * @param dataSourceName the dataSourceName to set
	 */
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	/**
	 * @return the event type
	 */
	public String getEventType() {
		return this.eventType;
	}

	/**
	 * @param eventType the event type to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the importName
	 */
	public String getImportName() {
		return importName;
	}

	/**
	 * @param importName the importName to set
	 */
	public void setImportName(String importName) {
		this.importName = importName;
	}

	public boolean isUsStyleDateFormat() {
		return usStyleDateFormat;
	}

	public void setUsStyleDateFormat(boolean usStyleDateFormat) {
		this.usStyleDateFormat = usStyleDateFormat;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Override
	public String toString() {
		return "ImportRecord [importId=" + importId + ", dataSourceId=" + dataSourceId + ", dataSourceName="
				+ dataSourceName + ", importName=" + importName + ", eventType=" + eventType + ", usStyleDateFormat="
				+ usStyleDateFormat + ", filePath=" + filePath + ", fileType=" + fileType + ", importColumnsToTypeMap="
				+ importColumnsToTypeMap + ",metadata=" + metadata + ", allowZipFiles=" + allowZipFiles + ", unprocessedUrlPath=" + unprocessedUrlPath + "]";
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the datasourceData
	 */
	public IngestDataSourceDTO getDatasourceData() {
		return datasourceData;
	}

	/**
	 * @param datasourceData the datasourceData to set
	 */
	public void setDatasourceData(IngestDataSourceDTO datasourceData) {
		this.datasourceData = datasourceData;
	}

	/**
	 * @return the datasourceType
	 */
	public String getDatasourceType() {
		return datasourceType;
	}

	/**
	 * @param datasourceType the datasourceType to set
	 */
	public void setDatasourceType(String datasourceType) {
		this.datasourceType = datasourceType;
	}

	/**
	 * @return the datasourceSubType
	 */
	public String getDatasourceSubType() {
		return datasourceSubType;
	}

	/**
	 * @param datasourceSubType the datasourceSubType to set
	 */
	public void setDatasourceSubType(String datasourceSubType) {
		this.datasourceSubType = datasourceSubType;
	}
	
	/**
	 * @return the metadata
	 */
	public HashMap<String, Object> getMetadata() {
		return metadata;
	}

	/**
	 * @param metadata the metadata to set
	 */
	public void setMetadata(final HashMap<String, Object> metadata) {
		this.metadata = metadata;
	}

	/**
	 * @return the allowZipFiles
	 */
	public boolean isAllowZipFiles() {
		return allowZipFiles;
	}

	/**
	 * @param allowZipFiles the allowZipFiles to set
	 */
	public void setAllowZipFiles(boolean allowZipFiles) {
		this.allowZipFiles = allowZipFiles;
	}

	/**
	 * @return the unprocessedUrlPath
	 */
	public String getUnprocessedUrlPath() {
		return unprocessedUrlPath;
	}

	/**
	 * @param unprocessedUrlPath the unprocessedUrlPath to set
	 */
	public void setUnprocessedUrlPath(String unprocessedUrlPath) {
		this.unprocessedUrlPath = unprocessedUrlPath;
	}
}
