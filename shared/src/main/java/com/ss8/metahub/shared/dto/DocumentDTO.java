package com.ss8.metahub.shared.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "metadata")
public class DocumentDTO {

	public DocumentDTO() {
		super();
	}

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	@Id
	private String id;
	private Date eventDate;
	private Date ingestDate;
	private long datasourceId;
	private String datasourceName;
	private long importId;
	private String importName;
	private String eventType;
	private long intercept;
	private Map<String, String> metadataTextMap;
	private Map<String, Long> metadataLongMap;
	private Map<String, Double> metadataDoubleMap;
	private Map<String, Boolean> metadataBooleanMap;
	private Map<String, Date> metadatatimestampmap;
	private Map<String, List<LocationTuple>> metadataLocationMap;
	private List<LocationTuple> eventLocations;
	public DocumentDTO(String id) {
		super();
		this.id = id;
	}
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@Override
	public String toString() {
		return "DocumentDTO [id=" + id + ", eventDate=" + eventDate + ", ingestDate=" + ingestDate + ", datasourceId=" + datasourceId + ", datasourceName=" + datasourceName + ", importId=" + importId + ", importName=" + importName
				+ ", eventType=" + eventType + ", metadataTextMap=" + metadataTextMap + ", metadataLongMap=" + metadataLongMap + ", metadataDoubleMap=" + metadataDoubleMap + ", metadataBooleanMap=" + metadataBooleanMap
				+ ", metadatatimestampmap=" + metadatatimestampmap + ", eventLocations=" + eventLocations + ", metadataLocationMap=" + metadataLocationMap + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getIngestDate() {
		return ingestDate;
	}

	public void setIngestDate(Date ingestDate) {
		this.ingestDate = ingestDate;
	}

	public long getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(long datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(String datasourceName) {
		this.datasourceName = datasourceName;
	}

	public long getImportId() {
		return importId;
	}

	public void setImportId(long importId) {
		this.importId = importId;
	}

	public String getImportName() {
		return importName;
	}

	public void setImportName(String importName) {
		this.importName = importName;
	}

	public Map<String, String> getMetadataTextMap() {
		return metadataTextMap;
	}

	public void setMetadataTextMap(Map<String, String> metadataTextMap) {
		this.metadataTextMap = metadataTextMap;
	}

	public Map<String, Long> getMetadataLongMap() {
		return metadataLongMap;
	}

	public void setMetadataLongMap(Map<String, Long> metadataLongMap) {
		this.metadataLongMap = metadataLongMap;
	}

	public Map<String, Double> getMetadataDoubleMap() {
		return metadataDoubleMap;
	}

	public void setMetadataDoubleMap(Map<String, Double> metadataDoubleMap) {
		this.metadataDoubleMap = metadataDoubleMap;
	}

	public Map<String, Boolean> getMetadataBooleanMap() {
		return metadataBooleanMap;
	}

	public void setMetadataBooleanMap(Map<String, Boolean> metadataBooleanMap) {
		this.metadataBooleanMap = metadataBooleanMap;
	}
	
	public List<LocationTuple> getEventLocations() {
		return this.eventLocations;
	}

	public void setEventLocations(List<LocationTuple> eventLocations) {
		this.eventLocations = eventLocations;
	}

	public Map<String, Date> getMetadatatimestampmap() {
		return metadatatimestampmap;
	}

	public void setMetadatatimestampmap(Map<String, Date> metadatatimestampmap) {
		this.metadatatimestampmap = metadatatimestampmap;
	}

	/**
	 * @return the metadataLocationMap
	 */
	public Map<String, List<LocationTuple>> getMetadataLocationMap() {
		return metadataLocationMap;
	}

	/**
	 * @param metadataLocationMap the metadataLocationMap to set
	 */
	public void setMetadataLocationMap(Map<String, List<LocationTuple>> metadataLocationMap) {
		this.metadataLocationMap = metadataLocationMap;
	}

	/**
	 * @return the intercept
	 */
	public long getIntercept() {
		return intercept;
	}

	/**
	 * @param intercept the intercept to set
	 */
	public void setIntercept(long intercept) {
		this.intercept = intercept;
	}
}
