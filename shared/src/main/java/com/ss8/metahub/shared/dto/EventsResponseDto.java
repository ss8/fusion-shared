package com.ss8.metahub.shared.dto;

import java.util.List;

public class EventsResponseDto<T> {
    private List<T> data;
    private long lastRow;
    private List<String> secondaryColumns;
    
    public EventsResponseDto() {
    }

    public EventsResponseDto(List<T> data, long lastRow, List<String> secondaryColumns) {
        this.data = data;
        this.lastRow = lastRow;
        this.secondaryColumns = secondaryColumns;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getLastRow() {
        return lastRow;
    }

    public void setLastRow(long lastRow) {
        this.lastRow = lastRow;
    }

    public List<String> getSecondaryColumns() {
        return secondaryColumns;
    }

    public void setSecondaryColumns(List<String> secondaryColumns) {
        this.secondaryColumns = secondaryColumns;
    }
}