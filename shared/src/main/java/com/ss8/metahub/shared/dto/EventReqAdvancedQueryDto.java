package com.ss8.metahub.shared.dto;

import java.util.HashSet;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "startRow", "endRow", "sortModel", "iql" })

public class EventReqAdvancedQueryDto {

	@JsonProperty("startRow")
	private Integer startRow;
	@JsonProperty("endRow")
	private Integer endRow;
	@JsonProperty("sortModel")
	private SortModel sortModel;
	@JsonProperty("iql")
	private Iql iql;
	
	private HashSet<String> selectedColumns = new HashSet<String>();

	@JsonProperty("rowGroupCols")
	private List<RowGroupCol> rowGroupCols;

	@JsonProperty("selectedIntercepts")
	private List<Long> selectedIntercepts;
	
	@JsonProperty("selectedIntercepts")
	public List<Long> getSelectedIntercepts() {
		return selectedIntercepts;
	}

	@JsonProperty("selectedIntercepts")
	public void setSelectedIntercepts(List<Long> selectedIntercepts) {
		this.selectedIntercepts = selectedIntercepts;
	}
	
	@JsonProperty("startRow")
	public Integer getStartRow() {
		return startRow;
	}

	@JsonProperty("startRow")
	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	@JsonProperty("endRow")
	public Integer getEndRow() {
		return endRow;
	}

	@JsonProperty("endRow")
	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	@JsonProperty("iql")
	public Iql getIql() {
		return iql;
	}

	@JsonProperty("iql")
	public void setIql(Iql iql) {
		this.iql = iql;
	}

	@JsonProperty("rowGroupCols")
	public List<RowGroupCol> getRowGroupCols() {
		return rowGroupCols;
	}

	@JsonProperty("rowGroupCols")
	public void setRowGroupCols(List<RowGroupCol> rowGroupCols) {
		this.rowGroupCols = rowGroupCols;
	}
	
	@JsonProperty("sortModel")
	public SortModel getSortModel() {
		return sortModel;
	}

	@JsonProperty("sortModel")
	public void setSortModel(SortModel sortModel) {
		this.sortModel = sortModel;
	}

	/**
	 * @return the selectedColumns
	 */
	public HashSet<String> getSelectedColumns() {
		return selectedColumns;
	}

	/**
	 * @param selectedColumns the selectedColumns to set
	 */
	public void setSelectedColumns(HashSet<String> selectedColumns) {
		this.selectedColumns = selectedColumns;
	}
	
}
