package com.ss8.metahub.shared.dto;

public class DataSourceTypeDTO {

	private int id;
	private String datasourceType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDatasourceType() {
		return datasourceType;
	}

	public void setDatasourceType(String datasourceType) {
		this.datasourceType = datasourceType;
	}
}
