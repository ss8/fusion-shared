package com.ss8.metahub.shared.util;

import java.time.format.DateTimeFormatter;

public final class GlobalConstants {

	public static final String DATA_TYPE_TEXT = "Text";

	public static final String DATA_TYPE_DATE = "DateTime";
	
	public static final String DATA_TYPE_LONG_TEXT = "LongText";

	public static final String DATA_TYPE_BOOLEAN = "Boolean";

	public static final String DATA_TYPE_NUMBER = "Number";
	
	public static final String DATA_TYPE_GEO_POINT = "GeoPoint";
	
	public static final String DATA_TYPE_GEO_SHAPE = "GeoShape";
	
	public static final String DATA_TYPE_CELLID = "CellId";	
	
	public static String ELASTIC_INDEX_NAME = "metadata";

	public static String CASSANDRA_TABLE_NAME = "metadata";

	public static String FIELD_EVENT_TYPE = "eventType";
	
	public static String FIELD_ID = "id";

	public static String FIELD_EVENT_DATE = "eventDate";

	public static String FIELD_INGEST_DATE = "ingestDate";

	public static String FIELD_DATA_SOURCE_ID = "datasourceId";
	
	public static String FIELD_DATA_SOURCE_TYPE = "datasourceType";

	public static String FIELD_DATA_SOURCE_NAME = "datasourceName";

	public static String FIELD_IMPORT_ID = "importId";

	public static String FIELD_IMPORT_NAME = "importName";
	
	public static String FIELD_INTERCEPT = "intercept";
	
	public static String FIELD_INTERCEPT_NAME = "interceptName";
	
	public static String FIELD_SOI_NAME = "soiName";
	
	public static String FIELD_SOI = "soi";
	
	public static String FIELD_CASES = "cases";
	
	public static String FIELD_FTP_ACCOUNT = "ftpAccount";

	public static String FIELD_EVENT_LOCATIONS = "eventLocations";
	
	public static String FIELD_IMSI = "IMSI";
	
	public static String FIELD_IMEI = "IMEI";
	
	public static String FIELD_MSISDN = "MSISDN";
	
	public static String FIELD_LIID = "LIID";
	
	public static String FIELD_EVENT_ID = "eventId";
	
	public static String FIELD_CREATION_DATE = "creationDate";
	
	public static String FIELD_START_TIME = "startTime";
	
	public static String FIELD_END_TIME = "endTime";
	
	public static String FIELD_REQUEST_ID = "requestId";
	
	public static String FIELD_COUNT = "count";
	
	public static String FIELD_CORRELATION = "correlation";
	
	public static String FIELD_NAME = "name";
	
	public static String FIELD_METADATA_LOCATIONMAP = "metadataLocationMap";
	
	public static String FIELD_GLOBAL_SEARCH_TEXT = "globalSearchText";
	
	public static String FIELD_SENDER = "sender";
	
	public static String FIELD_RECIPIENT = "recipient";

	public static String FIELD_DOC_ID = "id";
	
	public static String FIELD_IDOSSIER = "idossier";
	
	public static String DISPLAY_NAME_FIELD_IDOSSIER = "Idossier";
	
	public static String FIELD_AREA = "area";
	
	public static String DISPLAY_NAME_FIELD_AREA = "Area";
	
	public static String FIELD_PHYSICAL_ADDRESS = "physicalAddress";
	
	public static String FIELD_LATITUDE = "latitude";
	
	public static String FIELD_LONGITUDE = "longitude";
	
	public static String FIELD_GEOMETRY = "geometry";
	
	public static String AZIMUTH = "azimuth";
	
	public static String WIDTH = "width";
	
	public static String RANGE = "range";
	
	public static String GEOMETRY_TYPE = "type";
	
	public static String GEOMETRY_COORDINATES = "coordinates";
	
	public static String GEOMETRY_POLYGON = "polygon";
	
	public static String DOCUMENT_EVENT_TYPE = "Document";
	
	public static String IMAGE_EVENT_TYPE = "Image";
	
	public static String AUDIO_EVENT_TYPE = "Audio";
	
	public static String SD_AUDIO_EVENT_TYPE = "SD_Audio";
	
	public static String SD_LOCATION_EVENT_TYPE = "SD_Location";
	
	public static String FIELD_DOMAIN = "domain";
	
	public static String FIELD_LOCKED = "locked";
	
	public static String FIELD_DNS_NAME = "dnsName";

	public final static String LEGACY_IMPORT_METADATA_URL = "legacyImportMetadataUrl";
	
	public static String FIELD_SD_AUDIO_METADATA = "sdAudioMetadata";

	public static String FIELD_SD_AUDIO_PRODUCTS = "sdAudioProducts";

	public static String FIELD_SD_LOCATIONS = "sdLocations";
	
	public static String AND_OPERATOR = "and";
	
	public static String OR_OPERATOR = "or";
	
	public static String EQUALS = "equals";
	
	public static final String HEADER_XSECRET = "X-secret";

	public static final String AUTH_HEADER_X_USERINFO_KEY = "X-Userinfo";
	
	public static final String AUTH_HEADER_X_AUTHTOKEN_KEY = "X-Auth-Token";

	public static String FIELD_PRODUCT_LOCKOUT = "productLockout_txt";
	
	public static String PRODUCT_LOCKOUT_VALUE_LOCKOUT = "Lockout";
	
	public static String FIELD_CAPTURE_ID = "captureId";
	
	public static final String MANDATORY_LOCATION_FIELDS[] = { 
		FIELD_EVENT_DATE, 
		FIELD_EVENT_LOCATIONS,
		FIELD_IMSI,
		FIELD_IMEI,
		FIELD_MSISDN,
		FIELD_LIID
	};
	
	public static final String MANDATORY_REGULAR_FIELDS[] = { 
		FIELD_EVENT_DATE 
	};
	
	public static final String BSA_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final DateTimeFormatter BSA_DATE_FORMATTER = DateTimeFormatter.ofPattern(BSA_DATE_FORMAT);
	
	public final static String UPLOAD_FILE_TYPE_CSV = "CSV";
	public final static String UPLOAD_FILE_TYPE_JSON = "JSON";
	public final static String UPLOAD_FILE_TYPE_XML = "XML";
	public final static String UPLOAD_FILE_TYPE_XLSX = "XLSX";
	public final static String UPLOAD_FILE_TYPE_PDF = "PDF";
	public final static String UPLOAD_FILE_TYPE_JPEG = "JPEG";
	public final static String UPLOAD_FILE_TYPE_JPG = "JPG";
	public final static String UPLOAD_FILE_TYPE_PNG = "PNG";
	public final static String UPLOAD_FILE_TYPE_GIF = "GIF";
	public final static String UPLOAD_FILE_TYPE_WEBP = "WEBP";
	public final static String UPLOAD_FILE_TYPE_DOC = "DOC";
	public final static String UPLOAD_FILE_TYPE_DOCX = "DOCX";
	public final static String UPLOAD_FILE_TYPE_KML = "KML";
	public final static String UPLOAD_FILE_TYPE_TXT = "TXT";
	public final static String UPLOAD_FILE_TYPE_XLS = "XLS";
	public final static String UPLOAD_FILE_TYPE_ZIP = "ZIP";

	public static final String ISO_UTC_FORMAT ="yyyy-MM-dd'T'HH:mm:ss'Z'";
	
	public static String GEOPOLY = "geoPoly";
	
	public static final String EXCLUDE_FIELDS[] = { 
			FIELD_IDOSSIER, FIELD_AREA
		};
	
}
