package com.ss8.metahub.shared.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;
import org.springframework.data.cassandra.core.cql.WriteOptions;
import org.springframework.stereotype.Service;

import com.ss8.metahub.shared.dao.DocumentDAO;
import com.ss8.metahub.shared.dto.DocumentDTO;
import com.ss8.metahub.shared.service.CassandraDataService;

@Service
public class CassandraDataServiceImpl implements CassandraDataService {
	
	private static final Logger logger = LoggerFactory
			.getLogger(CassandraDataServiceImpl.class);

	@Autowired
	DocumentDAO documentDAO;
	
	@Autowired
	private CassandraOperations cassandraTemplate;
	
	@Value("${metahub.cassandra.batching:false}")
	private boolean batchingEnabled;
	
	@Value("${metahub.cassandra.batchSize:750}")
	private int cassandraBatchSize;
	
	private static final int DAYS_TO_SECONDS = 24 * 60 * 60;

	@Override
	public void addData(final List<DocumentDTO> data, final int ttlInDays) {
		if (logger.isTraceEnabled()) {
			logger.trace("data to add: " + data);
		}
		
		if (batchingEnabled) {
			if (data.size() > cassandraBatchSize) {
				LinkedList<DocumentDTO> batchList = new LinkedList<DocumentDTO>();
				for (int index = 0; index < data.size(); index++) {
					if ((index + 1) % cassandraBatchSize == 0) {
						insertBatchToCassandra(ttlInDays, batchList);
						batchList.clear();
					} else {
						batchList.add(data.get(index));
					}
				}
				if (batchList.size() > 0) {
					insertBatchToCassandra(ttlInDays, batchList);
					batchList.clear();
				}
			} else {
				insertBatchToCassandra(ttlInDays, data);
			}
		} else {
			InsertOptions writeOptions = InsertOptions.builder().ttl(ttlInDays * DAYS_TO_SECONDS).build();
			for (DocumentDTO document : data) {
				cassandraTemplate.insert(document, writeOptions);
			}
		}
	}

	private void insertBatchToCassandra(final int ttlInDays, List<DocumentDTO> batchList) {
		CassandraBatchOperations batchOps = cassandraTemplate.batchOps(); 
		batchOps.insert(batchList, WriteOptions.builder().ttl(ttlInDays * DAYS_TO_SECONDS).build());
		batchOps.execute();
	}

	private Field[] fields = DocumentDTO.class.getDeclaredFields();

	@Override
	public void deleteData(List<String> ids) {
		for (String id : ids) {
			documentDAO.deleteById(id);
		}
	}

	@Override
	public List<Map<String, Object>> getData(List<String> ids) throws IllegalArgumentException, IllegalAccessException {
		List<DocumentDTO> data = documentDAO.findAllById(ids);
		List<Map<String, Object>> list = new ArrayList<>();
		for (DocumentDTO document : data)
			list.add(convertDTOtoMap(document));
		return list;

	}
	
	@Override
	public List<DocumentDTO> getDocumentDTOData(List<String> ids) throws IllegalArgumentException, IllegalAccessException {
		List<DocumentDTO> data = documentDAO.findAllById(ids);
		return data;
	}


	@Override
	public List<Map<String, Object>> getAllData() throws IllegalArgumentException, IllegalAccessException {
		List<DocumentDTO> data = documentDAO.findAll();
		List<Map<String, Object>> list = new ArrayList<>();
		for (DocumentDTO document : data)
			list.add(convertDTOtoMap(document));
		return list;
	}

	public Map<String, Object> convertDTOtoMap(DocumentDTO document)
			throws IllegalArgumentException, IllegalAccessException {

		Map<String, Object> map = new HashMap<>();
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.get(document) instanceof Map<?, ?>) {
				Map<?, ?> m = (Map<?, ?>) field.get(document);
				for (Object s : m.keySet()) {
					if (null != m.get(s)) {
						String key = (String) s;
						map.put(key, m.get(s));
					}
				}
			} else if (null != field.get(document)) {
				map.put(field.getName(), field.get(document));
			}
		}
		return map;
	}

	@Override
	public void deleteData() {
		documentDAO.deleteAll();
	}
}
