package com.ss8.metahub.shared.dto;

import java.util.Date;

public class SDAudioProduct {
	
	private String productUrl;
	
	private String fileName;
	
	private Date startTime;
	
	private Date endTime;
	
	private String highestDecibelLevelColor;
	
	private long size;
	
	public SDAudioProduct() {
	}
	
	public SDAudioProduct(final String productUrl, final String fileName, final Date startTime, final Date endTime, final String highestDecibelLevelColor, final long size) {
		this.productUrl = productUrl;
		this.fileName = fileName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.highestDecibelLevelColor = highestDecibelLevelColor;
		this.size = size;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the highestDecibelLevelColor
	 */
	public String getHighestDecibelLevelColor() {
		return highestDecibelLevelColor;
	}

	/**
	 * @param highestDecibelLevelColor the highestDecibelLevelColor to set
	 */
	public void setHighestDecibelLevelColor(String highestDecibelLevelColor) {
		this.highestDecibelLevelColor = highestDecibelLevelColor;
	}

	/**
	 * @return the productUrl
	 */
	public String getProductUrl() {
		return productUrl;
	}

	/**
	 * @param productUrl the productUrl to set
	 */
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}
	
	@Override
    public String toString() {
        return String.format("SDAudioProduct{fileName='%s', startTime='%s', endTime='%s', productUrl='%s', highestDecibelLevelColor='%s', size='%s'}", 
        		fileName, startTime, endTime, productUrl, highestDecibelLevelColor, size);
    }

}
