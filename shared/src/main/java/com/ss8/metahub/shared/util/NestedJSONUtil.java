package com.ss8.metahub.shared.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class NestedJSONUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();
    
    public static LinkedHashMap<String, Object> convertToFlatMap(String keyPrefix, Object parsedJsonObj, int maxLevel) throws JsonProcessingException {
        
        LinkedHashMap<String, Object> flatMap = new LinkedHashMap<>();
        parseJsonLevel(keyPrefix, parsedJsonObj, 0, flatMap, maxLevel);
        
        return flatMap;
    }


    private static void parseJsonLevel(String keyPrefix, Object parsedJsonObj, int currLevel, Map<String, Object> flatMap, int maxLevel) throws JsonProcessingException {
        if(currLevel == maxLevel) {
            String objStr = objectMapper.writeValueAsString(parsedJsonObj);
            checkTypeAndParse(keyPrefix, objStr, currLevel, flatMap, maxLevel);
            return;
        }
        if(parsedJsonObj instanceof Map) {
            Map<String, Object> currMap = (Map<String, Object>) parsedJsonObj;
            // loop through all the keys in the map
            for(String key : currMap.keySet()) {
                String currKey = key;
                if(keyPrefix.length() > 0) {
                    currKey = keyPrefix + "." + key;
                }
                Object val = currMap.get(key);
                // Check if for the key there is nested object (converted to hashmap by object mapper when parsing)
                checkTypeAndParse(currKey, val, currLevel, flatMap, maxLevel);
            }
        } else if(parsedJsonObj instanceof List) {
            String objStr = objectMapper.writeValueAsString(parsedJsonObj);
            checkTypeAndParse(keyPrefix, objStr, currLevel, flatMap, maxLevel);
            //Below for loop handles array. If array needs to be handled, then uncomment below
            // code and comment above two lines.
            /*for(Object currObj : (List) parsedJsonObj) {
                checkTypeAndParse(keyPrefix, currObj, currLevel, flatMap, maxLevel);
            }*/
        }

    }

    private static void checkTypeAndParse(String currKey, Object val, int currLevel, Map<String, Object> flatMap, int maxLevel) throws JsonProcessingException {
        if(val instanceof Map || val instanceof List) {
            // call method recursively to parse the nested json
            parseJsonLevel(currKey, val, currLevel+1, flatMap, maxLevel);
        } else {
            // if the new nested key is not present in flatmap add the value in flatmap
            if(flatMap.get(currKey) == null) {
                flatMap.put(currKey, val);
            } else {
                // if value is present for nested key in flatmap, we are dealing with an array
                Object oldVal = flatMap.get(currKey);
                if(oldVal instanceof List) {
                    ((List) flatMap.get(currKey)).add(val);
                } else {
                    List listObj = new ArrayList();
                    listObj.add(oldVal);
                    listObj.add(val);
                    flatMap.put(currKey, listObj);
                }
            }
        }
    }
}
