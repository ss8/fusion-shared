package com.ss8.metahub.shared.dto;

public class ImportStatusDTO {
	
	private ImportStatus status;
	
	private String errorMessage;
	
	private Long failedRecordCount;
	
	private Long totalRecordCount;

	/**
	 * @return the status
	 */
	public ImportStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ImportStatus status) {
		this.status = status;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the failedRecordCount
	 */
	public Long getFailedRecordCount() {
		return failedRecordCount;
	}

	/**
	 * @param failedRecordCount the failedRecordCount to set
	 */
	public void setFailedRecordCount(Long failedRecordCount) {
		this.failedRecordCount = failedRecordCount;
	}

	/**
	 * @return the totalRecordCount
	 */
	public Long getTotalRecordCount() {
		return totalRecordCount;
	}

	/**
	 * @param totalRecordCount the totalRecordCount to set
	 */
	public void setTotalRecordCount(Long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

}
