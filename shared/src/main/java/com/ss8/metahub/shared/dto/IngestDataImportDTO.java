package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "description", "datasource", "filename", "status", "userName", "createdDate", "updatedDate", "datasourceData" ,"interceptName","raw"})
public class IngestDataImportDTO {

	@JsonProperty("id")
	private Long id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@Override
	public String toString() {
		return "IngestDataImportDTO [id=" + id + ", name=" + name + ", description=" + description + ", datasource=" + datasource + ", filename=" + filename + ", status=" + status + ", userName=" + userName + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + ", datasourceData=" + datasourceData + ", additionalProperties=" + additionalProperties +  ", interceptName=" + interceptName +  ", raw=" + raw +"]";
	}

	@JsonProperty("datasource")
	private Integer datasource;
	@JsonProperty("filename")
	private String filename;
	@JsonProperty("status")
	private String status;
	@JsonProperty("userName")
	private String userName;
	@JsonProperty("createdDate")
	private String createdDate;
	@JsonProperty("updatedDate")
	private String updatedDate;
	@JsonProperty("datasourceData")
	private IngestDataSourceDTO datasourceData;
	@JsonProperty("totalRecords")
	private int totalRecords;
	@JsonProperty("error")
	private String error;
	@JsonProperty("rowcount")
	private Long rowCount;
	@JsonProperty("failedrowcount")
	private Long failedRowCount;

    @JsonProperty("interceptName")
    private String interceptName;

    @JsonProperty("raw")
    private boolean raw;

    @JsonProperty("interceptName")
    public String getInterceptName() {
        return interceptName;
    }

    @JsonProperty("interceptName")
    public void setInterceptName(String interceptName) {
        this.interceptName = interceptName;
    }

    @JsonProperty("raw")
    public boolean isRaw() {
        return raw;
    }

    @JsonProperty("raw")
    public void setRaw(boolean raw) {
        this.raw = raw;
    }

    @JsonProperty("datasourceData")
	public IngestDataSourceDTO getDatasourceData() {
		return datasourceData;
	}

	@JsonProperty("datasourceData")
	public void setDatasourceData(IngestDataSourceDTO datasourceData) {
		this.datasourceData = datasourceData;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("datasource")
	public Integer getDatasource() {
		return datasource;
	}

	@JsonProperty("datasource")
	public void setDatasource(Integer datasource) {
		this.datasource = datasource;
	}

	@JsonProperty("filename")
	public String getFilename() {
		return filename;
	}

	@JsonProperty("filename")
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("userName")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("userName")
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonProperty("createdDate")
	public String getCreatedDate() {
		return createdDate;
	}

	@JsonProperty("createdDate")
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@JsonProperty("updatedDate")
	public String getUpdatedDate() {
		return updatedDate;
	}

	@JsonProperty("updateDate")
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@JsonProperty("error")
	public String getError() {
		return error;
	}

	@JsonProperty("error")
	public void setError(String error) {
		this.error = error;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * @return the totalRecords
	 */
	public int getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the failedRowCount
	 */
	public Long getFailedRowCount() {
		return failedRowCount;
	}

	/**
	 * @param failedRowCount the failedRowCount to set
	 */
	public void setFailedRowCount(Long failedRowCount) {
		this.failedRowCount = failedRowCount;
	}
}
