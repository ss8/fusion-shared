package com.ss8.metahub.shared.dto;

public class DataSourceEventDTO {

	private int id;
	private String eventType;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
}
