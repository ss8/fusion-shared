package com.ss8.metahub.shared.util;

import java.util.ArrayList;
import java.util.List;

import com.ss8.metahub.shared.dto.GeoPoint;

public class PolygonCoordinates {
	
	/**
	 * 
	 * @param geoPoint - Initial GeoPoint object
	 * @param azimuth - Azimuth in degrees
	 * @param distance - Distance in kilometers
	 * @return a LonLat that is offset from initial LonLat object by distance km in the azimuth direction
	 */
	private static GeoPoint offset(GeoPoint geoPoint, double azimuth, double distance) {
		
		double az = Math.toRadians(azimuth);
		double lat = Math.toRadians(geoPoint.getLatitude());
		double lon = Math.toRadians(geoPoint.getLongitude());
		//Radius of the Earth in km. Assumes spherical Earth.
		double radius = 6378.10;
		double lat1 = Math.asin(Math.sin(lat) * Math.cos(distance/radius) + Math.cos(lat) * Math.sin(distance/radius) * Math.cos(az));
		double lon1 = lon + Math.atan2(Math.sin(az) * Math.sin(distance/radius) * Math.cos(lat), Math.cos(distance/radius) - Math.sin(lat) * Math.sin(lat1));
		return new GeoPoint(Math.toDegrees(lat1),Math.toDegrees(lon1));
		
	}
	
	/**
	 * 
	 * @param lon - The longitude in degrees of the tower
	 * @param lat - The latitude in degrees of the tower
	 * @param azimuth - The azimuth in degrees of the center line of the cell segment
	 * @param width - The arc width in degrees of the cell segment
	 * @param range - The distance in kilometers of the cell segment
	 * @return - A comma separated list of locations. The last location is the same as the first location to create a closed polygon
	 */

	public static List<GeoPoint> createPolygon(double lon, double lat, double azimuth, double width, double distance){
		
		GeoPoint geoPoint = new GeoPoint(lat, lon);
		//start azimuth
		double start_az = azimuth - width/2;
		//end azimuth
		double end_az = azimuth + width/2;
		// arc increment
		int arc_incr = 3;
		List<GeoPoint> lonLatList = new ArrayList<>();
		
		if (width < 360) {
			lonLatList.add(geoPoint);
		}
		
		while (start_az < end_az ) {
			lonLatList.add(offset(geoPoint, start_az, distance));
			start_az += arc_incr;
		}
		lonLatList.add(offset(geoPoint, end_az, distance));
		
		if (width < 360) {
			lonLatList.add(geoPoint);
		}

		return lonLatList;
		
	}
	
	public static void main(String[] args) {
		List<GeoPoint> lonLatList = createPolygon(-2.344281381,49.72509132,547,120,0.1);
		lonLatList.stream().forEach(l -> System.out.println(l.getLongitude() + "," + l.getLatitude()));
	}

}
