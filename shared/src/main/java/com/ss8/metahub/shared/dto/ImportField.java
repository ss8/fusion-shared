package com.ss8.metahub.shared.dto;

import java.util.List;

public class ImportField {
	
	private String fieldName;
	
	private String dataType;
	
	private String mappedField;
	
	private String dateTimeFormat;
	
	private String displayName;
	
	private List<String> fieldsToMerge;
	
	private String derivedField;
	
	private boolean mandatory;

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the mappedField
	 */
	public String getMappedField() {
		return mappedField;
	}

	/**
	 * @param mappedField the mappedField to set
	 */
	public void setMappedField(String mappedField) {
		this.mappedField = mappedField;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return the dateTimeFormat
	 */
	public String getDateTimeFormat() {
		return dateTimeFormat;
	}

	/**
	 * @param dateTimeFormat the dateTimeFormat to set
	 */
	public void setDateTimeFormat(String dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the fieldsToMerge
	 */
	public List<String> getFieldsToMerge() {
		return fieldsToMerge;
	}

	/**
	 * @param fieldsToMerge the fieldsToMerge to set
	 */
	public void setFieldsToMerge(List<String> fieldsToMerge) {
		this.fieldsToMerge = fieldsToMerge;
	}

	/**
	 * @return the derivedField
	 */
	public String getDerivedField() {
		return derivedField;
	}

	/**
	 * @param derived the derivedField to set
	 */
	public void setDerivedField(String derivedField) {
		this.derivedField = derivedField;
	}

	@Override
	public String toString() {
		return "ImportField [fieldName=" + fieldName + ", dataType=" + dataType + ", mappedField=" + mappedField
				+ ", dateTimeFormat=" + dateTimeFormat + ", displayName=" + displayName + ", fieldsToMerge="
				+ fieldsToMerge + ", derivedField=" + derivedField + "]";
	}
	
	
}
