package com.ss8.metahub.shared.dto;

import java.util.Arrays;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Repository
@JsonIgnoreProperties(ignoreUnknown = true)
public class BSAResponse {

	@JsonProperty("event_id")
	private String eventId;
	
	@JsonProperty("coordinates")
	private double[] coordinates;
	
	private String address;
	
	public BSAResponse() {
		super();
	}

	public BSAResponse(String eventId, double[] coordinates, String address) {
		super();
		this.eventId = eventId;
		this.coordinates = coordinates;
		this.address = address;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "BSAResponse [eventId=" + eventId + ", coordinates=" + Arrays.toString(coordinates) + ", address="
				+ address + "]";
	}

	
	

}