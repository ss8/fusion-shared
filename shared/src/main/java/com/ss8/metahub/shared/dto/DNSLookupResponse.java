package com.ss8.metahub.shared.dto;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Repository
@JsonIgnoreProperties(ignoreUnknown = true)
public class DNSLookupResponse {
	@JsonProperty("dnsName")
	private String dnsName;
	
	@JsonProperty("ip")
	private String ip;
	
	public DNSLookupResponse() {
		super();
	}

	public DNSLookupResponse(String dnsName, String ip) {
		super();
		this.dnsName = dnsName;
		this.ip = ip;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the dnsName
	 */
	public String getDnsName() {
		return dnsName;
	}

	/**
	 * @param dnsName the dnsName to set
	 */
	public void setDnsName(String dnsName) {
		this.dnsName = dnsName;
	}
}
