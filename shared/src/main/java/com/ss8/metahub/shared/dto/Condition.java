package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"operator",
"terms"
})
public class Condition {

@JsonProperty("operator")
private String operator;
@JsonProperty("terms")
private List<Term> terms = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("operator")
public String getOperator() {
return operator;
}

@JsonProperty("operator")
public void setOperator(String operator) {
this.operator = operator;
}

@JsonProperty("terms")
public List<Term> getTerms() {
return terms;
}

@JsonProperty("terms")
public void setTerms(List<Term> terms) {
this.terms = terms;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}