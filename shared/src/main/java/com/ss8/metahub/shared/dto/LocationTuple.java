package com.ss8.metahub.shared.dto;

import org.springframework.data.cassandra.core.mapping.Element;
import org.springframework.data.cassandra.core.mapping.Tuple;

@Tuple
public class LocationTuple {

	public LocationTuple(double latitude, double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Element(0)
	private double latitude;

	@Element(1)
	private double longitude;

	@Override
	public String toString() {
		return "LocationTuple [latitude=" + latitude + ", longitude=" + longitude + "]";
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
