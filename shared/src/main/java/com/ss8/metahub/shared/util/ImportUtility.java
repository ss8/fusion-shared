package com.ss8.metahub.shared.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.metahub.shared.dto.DNSLookupResponse;
import com.ss8.metahub.shared.dto.FieldDisplayName;
import com.ss8.metahub.shared.dto.ImportField;
import com.ss8.metahub.shared.dto.ImportRecord;
import com.ss8.metahub.shared.dto.Metadata;

public final class ImportUtility {
	
	public static Pattern VALID_FIELD_NAME_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_]+");
	
	public static final String METAHUB_DOMAIN = "MH";
	
	public static String getDynamicFieldNameForFieldAndType(final String field, final String dataType) {
		if (GlobalConstants.DATA_TYPE_BOOLEAN.equals(dataType)) {
			return field.toLowerCase() + "_bool";
		} else if (GlobalConstants.DATA_TYPE_DATE.equals(dataType)) {
			return field.toLowerCase() + "_dt";
		} else if (GlobalConstants.DATA_TYPE_NUMBER.equals(dataType)) {
			return field.toLowerCase() + "_db";
		} else if (GlobalConstants.DATA_TYPE_GEO_POINT.equals(dataType)) {
			return field.toLowerCase() + "_geo_pt";
		} else {
			return field.toLowerCase() + "_txt";
		}
	}
	
	public static String getValidFieldName(final String field) {
		return field.toLowerCase().replaceAll("[^A-Za-z0-9_]+", "").replaceAll("^[_0-9]+", "");
	}

	public static void updateMappedField(final LinkedHashMap<String, ImportField> csvFieldNamesWithTypeMapping,
			final List<FieldDisplayName> currentfields) {
		// Get all the display name to fields mapping from the csvFieldNamesWithTypeMapping.
		TreeSet<String> csvDisplayNameSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		HashMap<String, String> csvFieldToDynamicFieldMapping = new HashMap<String, String>();
		for (String field : csvFieldNamesWithTypeMapping.keySet()) {
			ImportField importField = csvFieldNamesWithTypeMapping.get(field);
			if (importField.getMappedField() == null || importField.getMappedField().isEmpty()) {
				if (VALID_FIELD_NAME_PATTERN.matcher(field).matches()) {
					if (importField.getDisplayName() != null) {
						csvDisplayNameSet.add(importField.getDisplayName());
					}
					csvFieldToDynamicFieldMapping.put(ImportUtility.getDynamicFieldNameForFieldAndType(field,
							csvFieldNamesWithTypeMapping.get(field).getDataType()), field);
				} else {
					// Convert the field name to valid one and also store it in the field to display name map.
					String validFieldName = ImportUtility.getDynamicFieldNameForFieldAndType(
							ImportUtility.getValidFieldName(field), importField.getDataType());
					importField.setMappedField(validFieldName);
				}
			}
		}
		for (FieldDisplayName fieldDisplayName : currentfields) {
			ImportField importField = csvFieldNamesWithTypeMapping.get(fieldDisplayName.getField());
			// Check whether the dynamic field for the csv column is same as the field
			// defined in the schema.
			// If so then set the mapped field on the import record of the csv column field
			// to the field defined in the schema.
			// Else if check whether any of the column names from the csv has display name
			// already defined, if so then use that column/field in the schema for the mapping.
			if (csvFieldToDynamicFieldMapping.containsKey(fieldDisplayName.getField())
					&& csvDisplayNameSet.contains(fieldDisplayName.getDisplayName())) {
				csvFieldNamesWithTypeMapping.get(csvFieldToDynamicFieldMapping.get(fieldDisplayName.getField()))
						.setMappedField(fieldDisplayName.getField());
			} else if (csvDisplayNameSet.contains(fieldDisplayName.getDisplayName()) && importField != null
					&& (importField.getDataType() == null
							|| fieldDisplayName.getDataType().equalsIgnoreCase(importField.getDataType()))) {
				importField.setMappedField(fieldDisplayName.getField());
			}
		}

		// Finally update all the unmapped fields to dynamic fields.
		for (String field : csvFieldNamesWithTypeMapping.keySet()) {
			ImportField importField = csvFieldNamesWithTypeMapping.get(field);
			if (importField.getMappedField() == null) {
				importField.setMappedField(ImportUtility.getDynamicFieldNameForFieldAndType(
						ImportUtility.getValidFieldName(field), importField.getDataType()));
			}
		}
	}
	
	public static LinkedHashMap<String, ImportField> getImportColumnsToTypeMap(final String jsonColumnsToMap) throws JsonMappingException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(jsonColumnsToMap);
		Iterator<Map.Entry<String, JsonNode>> fields = rootNode.fields();
		LinkedHashMap<String, ImportField> importColumnsToTypeMap = new LinkedHashMap<String, ImportField>();
		while (fields.hasNext()) {
			Map.Entry<String, JsonNode> entry = fields.next();
			if (entry.getKey() == null || entry.getKey().trim().isEmpty()) {
				continue;
			}
			JsonNode node = entry.getValue();
			ImportField importField = new ImportField();
			if (null != node.get("dataType")) {
				importField.setDataType(node.get("dataType").textValue());
			}
			if (null != node.get("dateTimeFormat")) {
				importField.setDateTimeFormat(node.get("dateTimeFormat").textValue());
			}
			importField.setFieldName(entry.getKey());
			if (null != node.get("mappedField")) {
				importField.setMappedField(node.get("mappedField").textValue());
			}
			if (null != node.get("displayName") && !node.get("displayName").textValue().trim().isEmpty()) {
				importField.setDisplayName(node.get("displayName").textValue());
			} else {
				importField.setDisplayName(ImportUtility.humanReadableDisplayName(entry.getKey()));
			}
			if (node.get("fieldsToMerge") != null && node.get("fieldsToMerge").isArray()) {
				LinkedList<String> fieldsToMerge = new LinkedList<String>();
				for (final JsonNode fieldToMerge : node.get("fieldsToMerge")) {
					fieldsToMerge.add(fieldToMerge.textValue());
			    }
				importField.setFieldsToMerge(fieldsToMerge);
			}
			if (null != node.get("derivedField")) {
				importField.setDerivedField(node.get("derivedField").textValue());
			}
			importColumnsToTypeMap.put(entry.getKey(), importField);
		}
		return importColumnsToTypeMap;	
	}
	
	@SuppressWarnings("unused")
	public static String humanReadableDisplayName(final String field) {
		Pattern pattern = Pattern.compile("([A-Z])([A-Z])([a-z])|([a-z])([A-Z])");
		Matcher matcher = pattern.matcher(field);
		String[] words = matcher.replaceAll("$1$4 $2$3$5").replace(".", " ").split(" ");
		int index = 0;
        StringBuilder humanReadableDisplayName = new StringBuilder();
        for (String word : words) {
        	if (index > 0) {
        		humanReadableDisplayName.append(' ');
        	}
        	humanReadableDisplayName.append(word.substring(0, 1).toUpperCase() + ((word.length() > 1) ? word.substring(1, word.length()) : ""));
        	index++;
        }
        return humanReadableDisplayName.toString();
	}
	
	public static Metadata createMetadata(final ImportRecord importRecord, final String id) {
		Metadata metadata = new Metadata();
		metadata.setId(id);
		Date ingestDate = new Date();
		ingestDate.setTime((ingestDate.getTime() / 1000) * 1000);
		metadata.setIngestDate(ingestDate);
		metadata.setDatasourceName(importRecord.getDataSourceName());
		metadata.setDatasourceId(importRecord.getDataSourceId());
		metadata.setEventType(importRecord.getEventType());
		metadata.setImportId(importRecord.getImportId());
		metadata.setImportName(importRecord.getImportName());
		metadata.setIntercept(0);
		metadata.setDatasourceType(importRecord.getDatasourceSubType());
		if (metadata.getMetadataTextMap() == null) {
			metadata.setMetadataTextMap(new HashMap<String, String>());
		}
		metadata.getMetadataTextMap().put(GlobalConstants.FIELD_DOMAIN, METAHUB_DOMAIN);
		if (importRecord.getMetadata() != null) {
			for (String field : importRecord.getMetadata().keySet()) {
				Object fieldValue = importRecord.getMetadata().get(field);
				if (GlobalConstants.FIELD_INTERCEPT.equals(field)) {
					metadata.setIntercept(((Number) fieldValue).longValue());
					continue;
				}
				// Ignore adding ftp account to the metadata.
				if (GlobalConstants.FIELD_FTP_ACCOUNT.equals(field)) {
					continue;
				}
				if (fieldValue instanceof String) {
					if (metadata.getMetadataTextMap() == null) {
						metadata.setMetadataTextMap(new HashMap<String, String>());
					}
					metadata.getMetadataTextMap().put(field, (String) fieldValue);
				} else if (fieldValue instanceof Date) {
					if (GlobalConstants.FIELD_EVENT_DATE.equals(field)) {
						metadata.setEventDate((Date) fieldValue);
					} else {
						if (metadata.getMetadatatimestampMap() == null) {
							metadata.setMetadatatimestampMap(new HashMap<String, Date>());
						}
						metadata.getMetadatatimestampMap().put(field, (Date) fieldValue);
					}
				} else if (fieldValue instanceof Double) {
					if (metadata.getMetadataDoubleMap() == null) {
						metadata.setMetadataDoubleMap(new HashMap<String, Double>());
					}
					metadata.getMetadataDoubleMap().put(field, (Double) fieldValue);
				} else if (fieldValue instanceof Boolean) {
					if (metadata.getMetadataBooleanMap() == null) {
						metadata.setMetadataBooleanMap(new HashMap<String, Boolean>());
					}
					metadata.getMetadataBooleanMap().put(field, (Boolean) fieldValue);
				}
			}
		}
		return metadata;
	}
	
	public static void updateDNSNameToIpOnMetadataList(final List<Metadata> metadataList,
			final List<DNSLookupResponse> dnsLookupResponseList, final HashSet<String> dnsLookupIpFieldsSet) {
		HashMap<String, String> ipToDnsNameMap = new HashMap<String, String>();
		for (DNSLookupResponse response : dnsLookupResponseList) {
			ipToDnsNameMap.put(response.getIp(), response.getDnsName());
		}
		for (Metadata metadata : metadataList) {
			if (metadata.getMetadataTextMap() != null) {
				for (String ipField : dnsLookupIpFieldsSet) {
					String ip = metadata.getMetadataTextMap().get(ipField);
					if (metadata.getMetadataTextMap().containsKey(ipField) 
							&& ipToDnsNameMap.containsKey(ip)) {
						if (metadata.getMetadataTextArrayMap() == null) {
							HashMap<String, List<String>> metadataTextArrayMap = new HashMap<String, List<String>>();
							metadata.setMetadataTextArrayMap(metadataTextArrayMap);
							List<String> textArrayList = new ArrayList<String>();
							metadataTextArrayMap.put(GlobalConstants.FIELD_DNS_NAME, textArrayList);
						}
						if (metadata.getMetadataTextArrayMap().get(GlobalConstants.FIELD_DNS_NAME) != null) {
							metadata.getMetadataTextArrayMap().get(GlobalConstants.FIELD_DNS_NAME).add(ipToDnsNameMap.get(ip));
						}
					}
				}
			}
		}
	}
}
