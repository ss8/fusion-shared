package com.ss8.metahub.shared.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EvidenceListDTO {

    @JsonProperty(value = "evidence_id")
    private Long id;
    
    private Long soiId;
    
    private String soiName;
    
    private String name;
    private String description;
    private Date dateCreated;
    private String createdBy;
    private Date dateUpdated;
    private String updatedBy;
    @JsonProperty(value = "event_id")
    private String eventId;
    private Integer errorCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the dateUpdated
	 */
	public Date getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * @param dateUpdated the dateUpdated to set
	 */
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
    
	
	/**
	 * @return the errorCode 
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public Long getSoiId() {
		return soiId;
	}

	public void setSoiId(Long soiId) {
		this.soiId = soiId;
	}

	@Override
	public String toString() {
		return "EvidenceListDTO [id=" + id + ", soiId=" + soiId + ", soiName="
				+ soiName + ", name=" + name + ", description=" + description
				+ ", dateCreated=" + dateCreated + ", createdBy=" + createdBy
				+ ", dateUpdated=" + dateUpdated + ", updatedBy=" + updatedBy
				+ ", eventId=" + eventId + ", errorCode=" + errorCode + "]";
	}

	public String getSoiName() {
		return soiName;
	}

	public void setSoiName(String soiName) {
		this.soiName = soiName;
	}
}
