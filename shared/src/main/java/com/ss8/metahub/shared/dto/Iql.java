package com.ss8.metahub.shared.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "version", "source", "queries" })
public class Iql {

	@JsonProperty("version")
	private String version;
	@JsonProperty("source")
	private String source;
	@JsonProperty("queries")
	private List<Query> queries = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	@JsonProperty("multiQueryConditions")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<MultiQueryCondition> multiQueryConditions = new ArrayList<MultiQueryCondition>();

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	@JsonProperty("version")
	public void setVersion(String version) {
		this.version = version;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	@JsonProperty("source")
	public void setSource(String source) {
		this.source = source;
	}

	@JsonProperty("queries")
	public List<Query> getQueries() {
		return queries;
	}

	@JsonProperty("queries")
	public void setQueries(List<Query> queries) {
		this.queries = queries;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * @return the multiQueryConditions
	 */
	public List<MultiQueryCondition> getMultiQueryConditions() {
		return multiQueryConditions;
	}

	/**
	 * @param multiQueryConditions the multiQueryConditions to set
	 */
	public void setMultiQueryConditions(List<MultiQueryCondition> multiQueryConditions) {
		this.multiQueryConditions = multiQueryConditions;
	}

}
