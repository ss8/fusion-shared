package com.ss8.metahub.shared.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Metadata {
	
	private String id;
	
	private String datasourceName;
	
	private long datasourceId;
	
	private Date eventDate;
	
	private Date ingestDate;
	
	private String eventType;
	
	private long importId;
	
	private String importName;
	
	private long intercept;
	
	private String datasourceType;
	
	private List<GeoPoint> eventLocations;
	
	private Map<String, String> metadataTextMap;
	
	private Map<String, Long> metadataLongMap;
	
	private Map<String, Double> metadataDoubleMap;
	
	private Map<String, Boolean> metadataBooleanMap;
	
	private Map<String, Date> metadatatimestampMap;
	
	private Map<String, List<GeoPoint>> metadataLocationMap;
	
	private Map<String, List<String>> metadataTextArrayMap;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the datasourceName
	 */
	public String getDatasourceName() {
		return datasourceName;
	}

	/**
	 * @param datasourceName the datasourceName to set
	 */
	public void setDatasourceName(String datasourceName) {
		this.datasourceName = datasourceName;
	}

	/**
	 * @return the datasourceId
	 */
	public long getDatasourceId() {
		return datasourceId;
	}

	/**
	 * @param datasourceId the datasourceId to set
	 */
	public void setDatasourceId(long datasourceId) {
		this.datasourceId = datasourceId;
	}

	/**
	 * @return the eventDate
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * @param eventdate the eventDate to set
	 */
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	/**
	 * @return the ingestDate
	 */
	public Date getIngestDate() {
		return ingestDate;
	}

	/**
	 * @param ingestDate the ingestDate to set
	 */
	public void setIngestDate(Date ingestDate) {
		this.ingestDate = ingestDate;
	}

	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the importId
	 */
	public long getImportId() {
		return importId;
	}

	/**
	 * @param importId the importId to set
	 */
	public void setImportId(long importId) {
		this.importId = importId;
	}

	/**
	 * @return the importName
	 */
	public String getImportName() {
		return importName;
	}

	/**
	 * @param importName the importName to set
	 */
	public void setImportName(String importName) {
		this.importName = importName;
	}

	/**
	 * @return the metadataTextMap
	 */
	public Map<String, String> getMetadataTextMap() {
		return metadataTextMap;
	}

	/**
	 * @param metadataTextMap the metadataTextMap to set
	 */
	public void setMetadataTextMap(Map<String, String> metadataTextMap) {
		this.metadataTextMap = metadataTextMap;
	}

	/**
	 * @return the metadataLongMap
	 */
	public Map<String, Long> getMetadataLongMap() {
		return metadataLongMap;
	}

	/**
	 * @param metadataLongMap the metadataLongMap to set
	 */
	public void setMetadataLongMap(Map<String, Long> metadataLongMap) {
		this.metadataLongMap = metadataLongMap;
	}

	/**
	 * @return the metadataDoubleMap
	 */
	public Map<String, Double> getMetadataDoubleMap() {
		return metadataDoubleMap;
	}

	/**
	 * @param metadataDoubleMap the metadataDoubleMap to set
	 */
	public void setMetadataDoubleMap(Map<String, Double> metadataDoubleMap) {
		this.metadataDoubleMap = metadataDoubleMap;
	}

	/**
	 * @return the metadataBooleanMap
	 */
	public Map<String, Boolean> getMetadataBooleanMap() {
		return metadataBooleanMap;
	}

	/**
	 * @param metadataBooleanMap the metadataBooleanMap to set
	 */
	public void setMetadataBooleanMap(Map<String, Boolean> metadataBooleanMap) {
		this.metadataBooleanMap = metadataBooleanMap;
	}

	/**
	 * @return the metadatatimestampMap
	 */
	public Map<String, Date> getMetadatatimestampMap() {
		return metadatatimestampMap;
	}

	/**
	 * @param metadatatimestampMap the metadatatimestampMap to set
	 */
	public void setMetadatatimestampMap(Map<String, Date> metadatatimestampMap) {
		this.metadatatimestampMap = metadatatimestampMap;
	}

	/**
	 * @return the eventLocations
	 */
	public List<GeoPoint> getEventLocations() {
		return eventLocations;
	}

	/**
	 * @param eventLocations the eventLocations to set
	 */
	public void setEventLocations(List<GeoPoint> eventLocations) {
		this.eventLocations = eventLocations;
	}

	/**
	 * @return the metadataLocationMap
	 */
	public Map<String, List<GeoPoint>> getMetadataLocationMap() {
		return metadataLocationMap;
	}

	/**
	 * @param metadataLocationMap the metadataLocationMap to set
	 */
	public void setMetadataLocationMap(Map<String, List<GeoPoint>> metadataLocationMap) {
		this.metadataLocationMap = metadataLocationMap;
	}

	/**
	 * @return the intercept
	 */
	public long getIntercept() {
		return intercept;
	}

	/**
	 * @param intercept the intercept to set
	 */
	public void setIntercept(long intercept) {
		this.intercept = intercept;
	}

	/**
	 * @return the datasourceType
	 */
	public String getDatasourceType() {
		return datasourceType;
	}

	/**
	 * @param datasourceType the datasourceType to set
	 */
	public void setDatasourceType(String datasourceType) {
		this.datasourceType = datasourceType;
	}

	/**
	 * @return the metadataTextArrayMap
	 */
	public Map<String, List<String>> getMetadataTextArrayMap() {
		return metadataTextArrayMap;
	}

	/**
	 * @param metadataTextArrayMap the metadataTextArrayMap to set
	 */
	public void setMetadataTextArrayMap(Map<String, List<String>> metadataTextArrayMap) {
		this.metadataTextArrayMap = metadataTextArrayMap;
	}
	
	@Override
	public String toString() {
		return "Metadata [id=" + id + ", datasourceName=" + datasourceName
				+ ", datasourceId=" + datasourceId + ", eventDate=" + eventDate
				+ ", ingestDate=" + ingestDate + ", eventType=" + eventType
				+ ", importId=" + importId + ", importName=" + importName
				+ ", intercept=" + intercept + ", soiProductLockout="
				+ ", datasourceType=" + datasourceType
				+ ", eventLocations=" + eventLocations + ", metadataTextMap="
				+ metadataTextMap + ", metadataLongMap=" + metadataLongMap
				+ ", metadataDoubleMap=" + metadataDoubleMap
				+ ", metadataBooleanMap=" + metadataBooleanMap
				+ ", metadatatimestampMap=" + metadatatimestampMap
				+ ", metadataLocationMap=" + metadataLocationMap
				+ ", metadataTextArrayMap=" + metadataTextArrayMap + "]";
	}

}
