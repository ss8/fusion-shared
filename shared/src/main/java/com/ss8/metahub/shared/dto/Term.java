package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "operator", "valueL", "valueR", "value" ,"values","terms","distance","metrics" })
public  class Term {

	@JsonProperty("values")
	public List<String> getValues() {
		return values;
	}
	
	@JsonProperty("values")
	public void setValues(List<String> values) {
		this.values = values;
	}

	@JsonProperty("name")
	private String name;
	@JsonProperty("operator")
	private String operator;
	@JsonProperty("valueL")
	private String valueL;
	@JsonProperty("valueR")
	private String valueR;
	@JsonProperty("value")
	private String value;
	@JsonProperty("terms")
	private List<Term> terms;
	@JsonProperty("values")
	public List<String> values;
	@JsonProperty("distance")
	private Double distance;
	@JsonProperty("metrics")
	private String metrics;
	
	@JsonProperty("distance")
	public Double getDistance() {
		return distance;
	}
	@JsonProperty("distance")
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	@JsonProperty("metrics")
	public String getMetrics() {
		return metrics;
	}
	@JsonProperty("metrics")
	public void setMetrics(String metrics) {
		this.metrics = metrics;
	}
	
	@JsonProperty("terms")
	public List<Term> getTerms() {
		return terms;
	}
	@JsonProperty("terms")
	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("operator")
	public String getOperator() {
		return operator;
	}

	@JsonProperty("operator")
	public void setOperator(String operator) {
		this.operator = operator;
	}

	@JsonProperty("valueL")
	public String getValueL() {
		return valueL;
	}

	@JsonProperty("valueL")
	public void setValueL(String valueL) {
		this.valueL = valueL;
	}

	@JsonProperty("valueR")
	public String getValueR() {
		return valueR;
	}

	@JsonProperty("valueR")
	public void setValueR(String valueR) {
		this.valueR = valueR;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
