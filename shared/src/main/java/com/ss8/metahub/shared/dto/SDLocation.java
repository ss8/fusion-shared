package com.ss8.metahub.shared.dto;

import java.util.Date;

public class SDLocation {
	
	public static final String GEO_POINT = "geoPoint";
	
	public static final String POLYGON = "polygon";
	
	public static final String LINE_STRING = "lineString";
	
	private String name;
	
	private String description;
	
	private Date timestamp;
	
	private byte locationType;
	
	private String coordinates;
	
	public SDLocation() {
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the coordinates
	 */
	public String getCoordinates() {
		return coordinates;
	}

	/**
	 * @param coordinates the coordinates to set
	 */
	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	/**
	 * @return the locationType
	 */
	public byte getLocationType() {
		return locationType;
	}

	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(byte locationType) {
		this.locationType = locationType;
	}
	
	@Override
    public String toString() {
        return String.format("SDLocation{name='%s', description='%s', timestamp=%s, locationType=%d, coordinates='%s'}", 
                             name, description, timestamp, locationType, coordinates);
    }
}
