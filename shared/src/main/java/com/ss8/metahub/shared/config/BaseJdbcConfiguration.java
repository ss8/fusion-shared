package com.ss8.metahub.shared.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaseJdbcConfiguration {
	
	@Value("${spring.datasource.hikari.connection-timeout:30000}")
	protected long connectionTimeout;

	@Value("${spring.datasource.hikari.maxLifeTime:1800000}")
	protected long maxLifeTime;
}
