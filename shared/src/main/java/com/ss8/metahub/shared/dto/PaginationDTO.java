package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.Map;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "startRow", "endRow",  "sortModel"})

public class PaginationDTO {


	@JsonProperty("startRow")
	private Integer startRow;
	@JsonProperty("endRow")
	private Integer endRow;
	@JsonProperty("sortModel")
	private SortModel sortModel;
	
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("startRow")
	public Integer getStartRow() {
		return startRow;
	}

	@JsonProperty("startRow")
	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	@JsonProperty("endRow")
	public Integer getEndRow() {
		return endRow;
	}

	@JsonProperty("endRow")
	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	

	@JsonProperty("sortModel")
	public SortModel getSortModel() {
		return sortModel;
	}

	@JsonProperty("sortModel")
	public void setSortModel(SortModel sortModel) {
		this.sortModel = sortModel;
	}

	

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}
	
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	

}
