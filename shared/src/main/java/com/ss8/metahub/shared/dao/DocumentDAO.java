package com.ss8.metahub.shared.dao;

import org.springframework.data.cassandra.repository.CassandraRepository;

import com.ss8.metahub.shared.dto.DocumentDTO;

public interface DocumentDAO extends CassandraRepository<DocumentDTO, String> {

}
