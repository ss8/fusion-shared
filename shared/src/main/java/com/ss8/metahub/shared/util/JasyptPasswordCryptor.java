package com.ss8.metahub.shared.util;

import java.security.Security;

import org.bouncycastle.jcajce.provider.BouncyCastleFipsProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.iv.RandomIvGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JasyptPasswordCryptor {

	private static final Logger log = LoggerFactory
			.getLogger(JasyptPasswordCryptor.class);

	private static final String PASSWORD = "security-analytics-ss8";

	private static String CIPHER_METHOD = "PBEWITHSHAAND128BITAES-BC";

	private StandardPBEStringEncryptor encryptor;

	private static class Holder {
		private static final JasyptPasswordCryptor INSTANCE = new JasyptPasswordCryptor();
	}

	private JasyptPasswordCryptor() {
		encryptor = new StandardPBEStringEncryptor();
		encryptor.setAlgorithm(CIPHER_METHOD);
		encryptor.setPassword(PASSWORD);
		encryptor.setIvGenerator(new RandomIvGenerator());
	}

	public static JasyptPasswordCryptor getInstance() {
		return Holder.INSTANCE;
	}

	public String encrypt(String property) {
		String encryptedWord = encryptor.encrypt(property);

		return encryptedWord;
	}

	public String decrypt(String property) {
		try {
			String decryptedWord = encryptor.decrypt(property);

			return decryptedWord;
		} catch (Exception ex) {
			log.error("error decrypting", ex);

			return property;
		}
	}

	public static void main(String[] args) throws Exception {
        // Add Bouncy Castle FIPS provider
        Security.addProvider(new BouncyCastleFipsProvider());

		String originalPassword = "P@ssw0rd*";
		if (args == null || args.length == 0)
			originalPassword = "ss8";
		else
			originalPassword = args[0];

		String encryptedPassword = JasyptPasswordCryptor.getInstance()
				.encrypt(originalPassword);
		String decryptedPassword = JasyptPasswordCryptor.getInstance()
				.decrypt(encryptedPassword);
		System.out.println("password Entered: " + originalPassword
				+ " -encrypt-" + encryptedPassword);
		System.out.println("decrpty-" + decryptedPassword);
	}
}
