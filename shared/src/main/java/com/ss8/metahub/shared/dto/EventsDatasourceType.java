package com.ss8.metahub.shared.dto;

public enum EventsDatasourceType {
	STRUCTURED, LOCATION, UNSTRUCTURED, LAWFUL_INTERCEPTION, LEGACY_WARRANT, SURVEILLANCE_DEVICE
}
