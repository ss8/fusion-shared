package com.ss8.metahub.shared.dto;

public class MultiQueryCondition {

	public static final String FOLLOWED_WITHIN = "FOLLOWED_WITHIN";

	public static final String BEFORE_OR_AFTER = "BEFORE_OR_AFTER";

	private String operator;

	private int proximityTimeInMins;

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @return the proximityTimeInMins
	 */
	public int getProximityTimeInMins() {
		return proximityTimeInMins;
	}

	/**
	 * @param proximityTimeInMins the proximityTimeInMins to set
	 */
	public void setProximityTimeInMins(int proximityTimeInMins) {
		this.proximityTimeInMins = proximityTimeInMins;
	}
}
