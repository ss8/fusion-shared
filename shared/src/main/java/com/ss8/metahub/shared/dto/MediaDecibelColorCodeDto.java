package com.ss8.metahub.shared.dto;

public class MediaDecibelColorCodeDto {
	
	private String productUrl;
	
	private String colorCode;
	
	private int errorCode = 0; 
	
	public MediaDecibelColorCodeDto() {
	}
	
	public MediaDecibelColorCodeDto(final String productUrl, final String colorCode) {
		this.productUrl = productUrl;
		this.colorCode = colorCode;
	}

	/**
	 * @return the productUrl
	 */
	public String getProductUrl() {
		return productUrl;
	}

	/**
	 * @param productUrl the productUrl to set
	 */
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	/**
	 * @return the colorCode
	 */
	public String getColorCode() {
		return colorCode;
	}

	/**
	 * @param colorCode the colorCode to set
	 */
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

}
