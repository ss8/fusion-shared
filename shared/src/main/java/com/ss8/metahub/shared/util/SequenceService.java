package com.ss8.metahub.shared.util;

public enum SequenceService {
	FILEPARSER(0), 
	STREAMING(1), 
	DPE(2),
	INDPIIM(3),
	DPE_QUELOADER_UNKNOWN(4),
	DPE_QUELOADER_IRI(5),
	DPE_QUELOADER_LUPD(6),
	DPE_QUELOADER_CSV(7),
	DPE_QUELOADER_METADATA(8),
	DPE_QUELOADER_IRI_KAFKA(9),
	RESERVED1(10),
	RESERVED2(11),
	RESERVED3(12),
	RESERVED4(13)
	;

	private int code;

	private SequenceService(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static SequenceService fromValue(int code) {
		for (SequenceService seq : SequenceService.values()) {
			if (seq.code == code) {
				return seq;
			}
		}

		throw new IllegalArgumentException("No SequenceService with code:  "
				+ code + " found");
	}
}
