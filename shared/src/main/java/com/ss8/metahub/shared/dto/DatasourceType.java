package com.ss8.metahub.shared.dto;

public enum DatasourceType {
	EVENTS_DATA_SOURCE, ENRICHMENT_DATA_SOURCE
}
