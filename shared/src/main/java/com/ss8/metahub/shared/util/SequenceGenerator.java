package com.ss8.metahub.shared.util;

import java.net.InetAddress;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SequenceGenerator {
	
	private static final Log log = LogFactory.getLog(SequenceGenerator.class);
	private int nodeId;

	private static final int TOTAL_BITS = 64;
	private static final int EPOCH_BITS = 42;
	private static final int NODE_ID_SERVICE_BITS = 5;
	private static final int NODE_ID_THREAD_BITS = 4;
	private static final int NODE_ID_HOST_BITS = 3;
	private static final int NODE_ID_BITS = NODE_ID_SERVICE_BITS + NODE_ID_THREAD_BITS + NODE_ID_HOST_BITS; // 12
	
	private static final int SEQUENCE_BITS = 10; 
	
	private static final int maxSequence = (int) (Math.pow(2, SEQUENCE_BITS) - 1); // 2 power 12 -1
	private static final int maxThread = (int) (Math.pow(2,  NODE_ID_THREAD_BITS) - 1);

	// Custom Epoch (Nov 21, 2019 Midnight UTC = 2019-11-21T00:00:00Z)
	private static final long CUSTOM_EPOCH = 1572566400000L;

	private volatile long lastTimestamp = -1L;
	private volatile long sequence = 1L;
	private static int nodeNum = 0;

	private static Map<Integer, SequenceGenerator> nodeSeqGenerator = new ConcurrentHashMap<Integer, SequenceGenerator>();
	
	static {
		try {
			InetAddress addr = InetAddress.getLocalHost();
			String hostName = addr.getHostName();
			nodeNum = getNodeNum(hostName);
			// Below is added as nodeNum cannot exceed 4095, if so, we need to start from 0. To support nodeNum greater than 4095, we need to increase NODE_ID_BITS value.
			if (nodeNum > 4095) {
				nodeNum = 0;
			}
		} catch (Exception e) {
			log.warn("Unable to determine host for node id. Will use default node id.", e);
		}		
	}
	
	private static int getNodeNum(String hostName) {
		try {
			if (log.isInfoEnabled()) {
				log.info("hostName: " + hostName);
			}
			
			int firstIndex = hostName.indexOf(".");
			if (firstIndex > 0) {
				hostName = hostName.substring(0, firstIndex);
			}
			String[] hostSplit = hostName.split("-");

			return Integer.parseInt(hostSplit[hostSplit.length - 1]);
		} catch (Exception e) {
			log.warn(
				"Unable to determine host for node id. Will use default node id.",
				e);
			return 0;
		}
	}
	
	public static int getNodeId(SequenceGeneratorInfo info) {
		return getNodeId(info.getService(), info.getThreadNum());
	}
	
	private static int getNodeId(SequenceService service, int threadNum) {
		if (threadNum < 0 || threadNum > maxThread) {
			throw new IllegalArgumentException("unsupported thread number: "
					+ threadNum);
		}
		
		int code = service.getCode();
		int nodeId = code << (NODE_ID_BITS - NODE_ID_SERVICE_BITS);
		nodeId = nodeId | (threadNum << (NODE_ID_BITS - NODE_ID_SERVICE_BITS - NODE_ID_THREAD_BITS));
		nodeId += nodeNum;
		
		return nodeId;
	}
	
	public static SequenceGenerator getInstance(SequenceGeneratorInfo info) {
		return getInstance(info.getService(), info.getThreadNum());
	}
	
	private static SequenceGenerator getInstance(SequenceService service,
			int threadNum) {
		int nodeId = getNodeId(service, threadNum);
		return getInstance(nodeId);
	}

	private static SequenceGenerator getInstance(int node) {
		if (nodeSeqGenerator.get(node) == null) {
			synchronized (SequenceGenerator.class) {
				if (nodeSeqGenerator.get(node) == null) {
					log.info("Creating instance for node id : " + node);
					nodeSeqGenerator.put(node, new SequenceGenerator(node));
					log.info("Created instance for node id : " + node + " = "
						+ nodeSeqGenerator.get(node).toString());
				}
			}
		}
		
		return nodeSeqGenerator.get(node);
	}

	private SequenceGenerator(int nodeId) {
		super();
		this.nodeId = nodeId;
	}

	private static long timestamp() {
		return Instant.now().toEpochMilli() - CUSTOM_EPOCH;
	}

	// Block and wait till next millisecond
	private long waitNextMillis(long currentTimestamp) {
		while (currentTimestamp == lastTimestamp) {
			currentTimestamp = timestamp();
		}
		return currentTimestamp;
	}

	public synchronized long nextId() {
		long currentTimestamp = 0L; 
		
		currentTimestamp= timestamp();
		if (currentTimestamp == lastTimestamp) {
			sequence = (sequence + 1) & maxSequence;
			if (sequence == 0) {
				// Sequence Exhausted, wait till next millisecond.
				currentTimestamp = waitNextMillis(currentTimestamp);
			}
		} else {
			// reset sequence to start with zero for the next millisecond
			sequence = 0;
		}
		lastTimestamp = currentTimestamp;
		
		long id = currentTimestamp << (TOTAL_BITS - EPOCH_BITS); // shift 64 -42
		id = id | (nodeId << (TOTAL_BITS - EPOCH_BITS - NODE_ID_BITS)); // shift 64 -42 - 12
		id = id | sequence;
		
		return id;
	}

	public static String testRun(final int ti) {
		long start = Instant.now().toEpochMilli();
		for (int i = 0; i < 500; ++i) {
			SequenceGenerator seqGenerator = SequenceGenerator.getInstance(ti/2);
			
			long id = seqGenerator.nextId();
			// System.out.println("i: " + i + ", id: " +id);
		}
		System.out.println("thread: " + ti + " took ms: "
			+ (Instant.now().toEpochMilli() - start));

		return "done";
	}
	
	public static void main(String[] args) throws InterruptedException {
		String hostName = "dcp-intellego-dpe-1.intellego-dpe.dcp-inxt.svc.ss8-k8s-cluster";
		System.out.println("got node id: " + getNodeNum(hostName)
			+ " for host: " + hostName);
		hostName = "dcp-intellego-dpe-2";
		System.out.println("got node id: " + getNodeNum(hostName)
			+ " for host: " + hostName);
		
		long start = Instant.now().toEpochMilli();
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		CompletionService<String> completionService = new ExecutorCompletionService<>(executorService);
		
		for (int i = 0; i < 10; ++i){
			final int ti = i;
			completionService.submit(()-> testRun(ti));
		}
		
		for (int i = 0; i < 10; ++i) {
			completionService.take();
		}
		
		executorService.shutdown();
		System.out.println("took ms: " + (Instant.now().toEpochMilli() - start));
	}

}
