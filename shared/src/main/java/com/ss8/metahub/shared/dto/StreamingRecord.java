package com.ss8.metahub.shared.dto;

import java.util.LinkedHashMap;

public class StreamingRecord {
	
	private LinkedHashMap<String, Object> metadataMap = new LinkedHashMap<String, Object>();
	
	/**
	 * @return the metadata map.
	 */
	public LinkedHashMap<String, Object> getMetadataMap() {
		return this.metadataMap;
	}

	/**
	 * @param metadatamap to set
	 */
	public void setMetadataMap(LinkedHashMap<String, Object> metadataMap) {
		this.metadataMap = metadataMap;
	}

	@Override
	public String toString() {
		return "StreammingRecord [metadataMap="
				+ metadataMap + "]";
	}
}
