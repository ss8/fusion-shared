package com.ss8.metahub.shared.dto;

public class GenericResponse {

    private String message;
    private int code;
    
    public GenericResponse() {
    }

    public GenericResponse(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
