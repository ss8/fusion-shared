package com.ss8.metahub.shared.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.KeyHolder;

public class BaseJdbcDao {
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;

	protected int updateEntity(String sql) {
		return jdbcTemplate.update(sql);
	}

	protected int addEntity(String sql) {
		return jdbcTemplate.update(sql);
	}
	
	protected int addEntity(PreparedStatementCreator creator, KeyHolder holder) {
		return jdbcTemplate.update(creator, holder);
	}

	/**
	 * Query for a list of objects using Spring's BeanPropertyRowMapper
	 * This can cause TypeMismatchException if a primitive field is NULL in the DB
	 * @param sql
	 * @param clazz
	 * @return
	 */
	protected <T> List<T> queryForList(String sql, Class<T> clazz) {
		return jdbcTemplate.query(sql, new BeanPropertyRowMapper<T>(clazz));
	}

	protected List<String> queryForList(String sql) {
		return jdbcTemplate.query(sql, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString(1);
			}
		});
	}

	/**
	 * Query for a list of objects using the DAO's custom row mapper
	 * 
	 * @param sql
	 * @param rowmapper
	 * @return
	 */
	protected <T> List<T> queryForList(String sql, RowMapper rowmapper) {
		return jdbcTemplate.query(sql, rowmapper);
	}
	
	protected <T> T queryForObject(String sql, Class<T> clazz) {
		return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<T>(clazz));
	}
	
protected class IntRowMapper implements RowMapper<Integer> {
		
		private String columnName;
		
		public IntRowMapper(String column) {
			super( );
			this.columnName = column;
		}

		@Override
		public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
			return (rs.getInt(columnName));
		}
	}
}
