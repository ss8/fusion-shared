package com.ss8.metahub.shared.dto;

public class GeoPoint {
	
	private double latitude;
	
	private double longitude;
	
	public GeoPoint() {
	}
	
	public GeoPoint(final double latitude, final double longitude) {
		this.setLatitude(latitude);
		this.setLongitude(longitude);
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitute the latitute to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
