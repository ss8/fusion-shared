package com.ss8.metahub.shared.dto;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Used for mapping/validating input Json request.
 */
@Repository
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoordinatesRequest {
	
	@JsonProperty("cell_id")
	private String cellId;
	@JsonProperty("cell_type")
	private String cellType;
	@JsonProperty("eventdate")
	private String eventdate;
	@JsonProperty("event_id")
	private String eventId;


	public CoordinatesRequest() {
		super();
	}


	public CoordinatesRequest(String cellId, String cellType, String eventdate, String eventId) {
		super();
		this.cellId = cellId;
		this.cellType = cellType;
		this.eventdate = eventdate;
		this.eventId = eventId;
	}

	public String getCellId() {
		return cellId;
	}


	public void setCellId(String cellId) {
		this.cellId = cellId;
	}


	public String getCellType() {
		return cellType;
	}


	public void setCellType(String cellType) {
		this.cellType = cellType;
	}


	public String getEventdate() {
		return eventdate;
	}


	public void setEventdate(String eventdate) {
		this.eventdate = eventdate;
	}
	

	public String getEventId() {
		return eventId;
	}


	public void setEventId(String eventId) {
		this.eventId = eventId;
	}


	@Override
	public String toString() {
		return "CoordinatesRequest [cellId=" + cellId + ", cellType=" + cellType + ", eventdate=" + eventdate
				+ ", eventId=" + eventId + "]";
	}

	

}
