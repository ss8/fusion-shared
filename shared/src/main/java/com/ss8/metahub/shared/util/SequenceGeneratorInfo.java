package com.ss8.metahub.shared.util;

public class SequenceGeneratorInfo {

	private SequenceService service;

	private int threadNum;

	public SequenceGeneratorInfo(SequenceService service, int threadNum) {
		this.service = service;
		this.threadNum = threadNum;
	}

	public SequenceService getService() {
		return service;
	}

	public void setService(SequenceService service) {
		this.service = service;
	}

	public int getThreadNum() {
		return threadNum;
	}

	public void setThreadNum(int threadNum) {
		this.threadNum = threadNum;
	}

}
