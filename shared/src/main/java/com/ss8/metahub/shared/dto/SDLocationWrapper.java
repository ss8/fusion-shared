package com.ss8.metahub.shared.dto;

import java.util.List;

public class SDLocationWrapper {
	
	private List<SDLocation> sdLocations;

	/**
	 * @return the sdLocations
	 */
	public List<SDLocation> getSdLocations() {
		return sdLocations;
	}

	/**
	 * @param sdLocations the sdLocations to set
	 */
	public void setSdLocations(List<SDLocation> sdLocations) {
		this.sdLocations = sdLocations;
	}
}
