package com.ss8.metahub.shared.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.AclEntry;
import java.nio.file.attribute.AclEntryPermission;
import java.nio.file.attribute.AclEntryType;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilesUtil {
	private static final Logger logger = LoggerFactory
			.getLogger(FilesUtil.class);

	private static class Holder {
		public static FilesUtil instance = new FilesUtil();
	}

	private FilesUtil() {
	}

	public static FilesUtil getInstance() {
		return Holder.instance;
	}

	private boolean isUnix() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("nix") || osName.contains("nux")
				|| osName.contains("aix");
	}

	private boolean isWindows() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("windows");
	}
	
	private List<String> getDirs(String dir) {
		String current = "";
		if (dir.startsWith("/")) {
			current = "/";
			dir = dir.substring(1);
		} else if (dir.startsWith(".")) {
			current = ".";
			dir = dir.substring(1);
		}
		
		String[] eles = dir.split("/");
		List<String> dirs = new ArrayList<>();
		for (String ele : eles) {
			if (ele.equals("")) {
				continue;
			}
			
			if (current.equals("")) {
				current = ele;
			} else if (current.equals("/")) {
				current = current + ele;
			} else {
				current = current + "/" + ele;
			}
			dirs.add(current);
		}
		
		return dirs;
	}
	
	private boolean createDirectory(String path) throws IOException {
		// Check if the folder exists
		Path dir = Paths.get(path);
		if (Files.exists(Paths.get(path))) {
			logger.warn("folder exists: " + path + ". Skip");

			return true;
		}

		Path created = Files.createDirectories(dir);

		if (isUnix()) {

			// Set permissions for the directory
			Set<PosixFilePermission> permissions = new HashSet<>();
			permissions.add(PosixFilePermission.OWNER_READ);
			permissions.add(PosixFilePermission.OWNER_WRITE);
			permissions.add(PosixFilePermission.OWNER_EXECUTE);

			permissions.add(PosixFilePermission.GROUP_READ);
			permissions.add(PosixFilePermission.GROUP_WRITE);
			permissions.add(PosixFilePermission.GROUP_EXECUTE);

			permissions.add(PosixFilePermission.OTHERS_READ);
			permissions.add(PosixFilePermission.OTHERS_EXECUTE);

			Set<PosixFilePermission> createdPermissions = Files
					.getPosixFilePermissions(created);
			if (logger.isTraceEnabled()) {
				logger.trace("created: " + dir + " with Permissions: "
						+ createdPermissions);
			}

			Files.setPosixFilePermissions(created, permissions);
			
			this.addSbit(dir.toString());

			if (logger.isTraceEnabled()) {
				logger.trace(dir + " with permissions: "
						+ Files.getPosixFilePermissions(created));
			}

			return true;
		} else if (isWindows()) {
			// Get the ACL file attribute view
			AclFileAttributeView aclFileAttributeView = Files
					.getFileAttributeView(created,
							AclFileAttributeView.class);

			if (aclFileAttributeView != null) {
				// Define the user principal (example: "Everyone")
				UserPrincipal user = created.getFileSystem()
						.getUserPrincipalLookupService()
						.lookupPrincipalByName("Everyone");

				// Define the ACL entry with desired permissions
				AclEntry entry = AclEntry.newBuilder()
						.setType(AclEntryType.ALLOW).setPrincipal(user)
						.setPermissions(AclEntryPermission.READ_DATA,
								AclEntryPermission.WRITE_DATA,
								AclEntryPermission.APPEND_DATA,
								AclEntryPermission.READ_ATTRIBUTES,
								AclEntryPermission.WRITE_ATTRIBUTES,
								AclEntryPermission.READ_ACL,
								AclEntryPermission.WRITE_ACL,
								AclEntryPermission.DELETE,
								AclEntryPermission.READ_NAMED_ATTRS,
								AclEntryPermission.WRITE_NAMED_ATTRS,
								AclEntryPermission.SYNCHRONIZE)
						.build();

				// Apply the ACL entry to the directory
				aclFileAttributeView
						.setAcl(Collections.singletonList(entry));
			}
			
			return true;
		}

		return false;
	}

	public Path createDirectoriesWithDefaultAttributes(Path dirPath)
			throws IOException {
		if (dirPath == null) {
			logger.error("dir is null");

			return null;
		}

		if (logger.isTraceEnabled()) {
			logger.trace("start createDirectoriesWithDefaultAttributes - dir: "
					+ dirPath.toString());
		}

		try {
			// Check if the folder exists
			if (Files.exists(dirPath)) {
				logger.warn("folder exists: " + dirPath.getFileName() + ". Skip");

				return dirPath;
			}
			
			List<String> dirs = getDirs(dirPath.toString());
			for (String dir : dirs) {
				File f = new File(dir);
				if (!f.exists()) {
					boolean success = this.createDirectory(dir);
					if (!success) {
						return null;
					}
				}
			}
			
			return dirPath;
		} finally {
			if (logger.isTraceEnabled()) {
				logger.trace(
						"done createDirectoriesWithDefaultAttributes - dir: "
								+ dirPath.toString());
			}
		}
	}
	
	
	private void addSbit(String directoryPath) {
		try {
			// Build the chmod command
			ProcessBuilder processBuilder = new ProcessBuilder("chmod", "ug+s",
					directoryPath);

			// Start the process
			Process process = processBuilder.start();

			// Wait for the process to complete
			long timeout = 5;
			TimeUnit unit = TimeUnit.SECONDS;
			process.waitFor(timeout, unit);

			logger.debug(
					"Setuid and setgid permissions added to the directory successfully.");
		} catch (IOException | InterruptedException e) {
			logger.error("failed to set sticky bit", e);
		}
	}

	public static void main(String[] args) throws IOException {
		String dir = "c:\\var\\metahub\\test1";
		Path created = FilesUtil.getInstance()
				.createDirectoriesWithDefaultAttributes(Paths.get(dir));
		System.out.println("created the directory: " + created);
	}
}
