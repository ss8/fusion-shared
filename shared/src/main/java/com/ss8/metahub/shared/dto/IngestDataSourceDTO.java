package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "desc", "eventType", "fileType", "streamType", "id", "userName", "createdDate" ,"importColumnsToTypeMap" ,"canDelete" ,"usStyleDateFormat", "metadataFileRequired", "allowZipFiles" , "raw"})

public class IngestDataSourceDTO {
	

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("desc")
	private String desc;
	@JsonProperty("eventType")
	private String eventType;
	@JsonProperty("userName")
	private String userName;
	@JsonProperty("fileType")
	private String fileType;
	@JsonProperty("createdDate")
	private String createdDate;
	@JsonProperty("importColumnsToTypeMap")
	private String importColumnsToTypeMap;
	@JsonProperty("totalRecords")
	private long totalRecords;
	@JsonProperty("canDelete")
	private boolean canDelete;
	@JsonProperty("usStyleDateFormat")
	private boolean usStyleDateFormat = false;
	@JsonProperty("rowcount")
	private Long rowCount;
	@JsonProperty("failedrowcount")
	private Long failedRowCount;
	@JsonProperty("streamType")
	private String streamType;
	@JsonProperty("datasourceType")
	private String datasourceType;
	@JsonProperty("metadataFileRequired")
	private boolean metadataFileRequired = false;
	@JsonProperty("allowZipFiles")
	private boolean allowZipFiles = false;

    @JsonProperty("raw")
    private boolean raw;

	private long importCount;
	
	private String datasourceFolderName;
	
	
	@JsonProperty("usStyleDateFormat")
	public boolean isUsStyleDateFormat() {
		return usStyleDateFormat;
	}
	@JsonProperty("usStyleDateFormat")
	public void setUsStyleDateFormat(boolean usStyleDateFormat) {
		this.usStyleDateFormat = usStyleDateFormat;
	}
	
	@JsonProperty("canDelete")
	public boolean isCanDelete() {
		return canDelete;
	}
	@JsonProperty("canDelete")
	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	@JsonProperty("importColumnsToTypeMap")
	public String getImportColumnsToTypeMap() {
		return importColumnsToTypeMap;
	}

	@JsonProperty("importColumnsToTypeMap")
	public void setImportColumnsToTypeMap(String importColumnsToTypeMap) {
		this.importColumnsToTypeMap = importColumnsToTypeMap;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("desc")
	public String getDesc() {
		return desc;
	}

	@JsonProperty("desc")
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@JsonProperty("eventType")
	public String getEventType() {
		return eventType;
	}

	@JsonProperty("eventType")
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@JsonProperty("userName")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("userName")
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@JsonProperty("fileType")
	public String getFileType() {
		return fileType;
	}
	
	@JsonProperty("fileType")
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	@JsonProperty("createdDate")
	public String getCreatedDate() {
		return createdDate;
	}

	@JsonProperty("createdDate")
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	@JsonProperty("streamType")
	public String getStreamType() {
		return streamType;
	}
	
	@JsonProperty("streamType")
	public void setStreamType(String streamType) {
		this.streamType = streamType;
	}
	
	@JsonProperty("datasourceType")
	public String getDatasourceType() {
		return datasourceType;
	}

	@JsonProperty("datasourceType")
	public void setDatasourceType(String datasourceType) {
		this.datasourceType = datasourceType;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * @return the totalRecords
	 */
	public long getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @param totalRecords the totalRecords to set
	 */
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	/**
	 * @return the datasourceFolderName
	 */
	public String getDatasourceFolderName() {
		return datasourceFolderName;
	}
	/**
	 * @param datasourceFolderName the datasourceFolderName to set
	 */
	public void setDatasourceFolderName(String datasourceFolderName) {
		this.datasourceFolderName = datasourceFolderName;
	}
	
	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the failedRowCount
	 */
	public Long getFailedRowCount() {
		return failedRowCount;
	}

	/**
	 * @param failedRowCount the failedRowCount to set
	 */
	public void setFailedRowCount(Long failedRowCount) {
		this.failedRowCount = failedRowCount;
	}
	/**
	 * @return the importCount
	 */
	public long getImportCount() {
		return importCount;
	}
	/**
	 * @param importCount the importCount to set
	 */
	public void setImportCount(long importCount) {
		this.importCount = importCount;
	}
	

	public String toString() {
		return "IngestDataSourceDTO [id=" + id + ", name=" + name + ", desc=" + desc + ", eventType=" + eventType
				+ ", userName=" + userName + ", createdDate=" + createdDate + ", importColumnsToTypeMap="
				+ importColumnsToTypeMap + ", totalRecords=" + totalRecords + ", canDelete=" + canDelete
				+ ", usStyleDateFormat=" + usStyleDateFormat + ", rowCount=" + rowCount + ", failedRowCount="
				+ failedRowCount + ", fileType=" + fileType + ", streamType=" + streamType + ", datasourceType=" + datasourceType + ", importCount=" + importCount + ", datasourceFolderName="
				+ datasourceFolderName + ", additionalProperties=" + additionalProperties + ", metadataFileRequired=" + metadataFileRequired + ", allowZipFiles=" + allowZipFiles + ", raw=" + raw + "]";
	}
	/**
	 * @return the metadataFileRequired
	 */
	@JsonProperty("metadataFileRequired")
	public boolean isMetadataFileRequired() {
		return metadataFileRequired;
	}
	/**
	 * @param metadataFileRequired the metadataFileRequired to set
	 */
	@JsonProperty("metadataFileRequired")
	public void setMetadataFileRequired(boolean metadataFileRequired) {
		this.metadataFileRequired = metadataFileRequired;
	}
	/**
	 * @return the allowZipFiles
	 */
	public boolean isAllowZipFiles() {
		return allowZipFiles;
	}
	/**
	 * @param allowZipFiles the allowZipFiles to set
	 */
	public void setAllowZipFiles(boolean allowZipFiles) {
		this.allowZipFiles = allowZipFiles;
	}

    public boolean isRaw() {
        return raw;
    }

    public void setRaw(boolean raw) {
        this.raw = raw;
    }
}
