package com.ss8.metahub.shared.dto;

public class IngestStatistics {

	private long totalIngestedDocsCount;

	private double averageTimeTaken;

	private long totalIterations;
	
	private long totalTimeTaken;
	
	private long averageBatchSize;
	
	public IngestStatistics() {
	}

	/**
	 * @return the totalIngestedDocsCount
	 */
	public long getTotalIngestedDocsCount() {
		return totalIngestedDocsCount;
	}

	/**
	 * @param totalIngestedDocsCount the totalIngestedDocsCount to set
	 */
	public void setTotalIngestedDocsCount(long totalIngestedDocsCount) {
		this.totalIngestedDocsCount = totalIngestedDocsCount;
	}

	/**
	 * @return the averageTimeTaken
	 */
	public double getAverageTimeTaken() {
		return averageTimeTaken;
	}

	/**
	 * @param averageTimeTaken the averageTimeTaken to set
	 */
	public void setAverageTimeTaken(double averageTimeTaken) {
		this.averageTimeTaken = averageTimeTaken;
	}

	/**
	 * @return the totalIterations
	 */
	public long getTotalIterations() {
		return totalIterations;
	}

	/**
	 * @param totalIterations the totalIterations to set
	 */
	public void setTotalIterations(long totalIterations) {
		this.totalIterations = totalIterations;
	}

	/**
	 * @return the totalTimeTaken
	 */
	public long getTotalTimeTaken() {
		return totalTimeTaken;
	}

	/**
	 * @param totalTimeTaken the totalTimeTaken to set
	 */
	public void setTotalTimeTaken(long totalTimeTaken) {
		this.totalTimeTaken = totalTimeTaken;
	}

	/**
	 * @return the averageBatchSize
	 */
	public long getAverageBatchSize() {
		return averageBatchSize;
	}

	/**
	 * @param averageBatchSize the averageBatchSize to set
	 */
	public void setAverageBatchSize(long averageBatchSize) {
		this.averageBatchSize = averageBatchSize;
	}
}
