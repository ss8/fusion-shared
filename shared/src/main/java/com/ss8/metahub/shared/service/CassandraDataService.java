package com.ss8.metahub.shared.service;

import java.util.List;
import java.util.Map;

import com.ss8.metahub.shared.dto.DocumentDTO;

public interface CassandraDataService {

	public void addData(List<DocumentDTO> data, final int ttlInDays);

	public void deleteData(List<String> ids);
	
	public void deleteData();

	public List<Map<String, Object>> getData(List<String> ids) throws IllegalArgumentException, IllegalAccessException;
	
	public List<Map<String, Object>> getAllData() throws IllegalArgumentException, IllegalAccessException;
	
	public List<DocumentDTO> getDocumentDTOData(List<String> ids) throws IllegalArgumentException, IllegalAccessException;

}
