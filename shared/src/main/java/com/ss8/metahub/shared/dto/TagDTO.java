package com.ss8.metahub.shared.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "scope", "status", "date_created", "date_modified", "description", "shortcut", "icon", "group" })
public class TagDTO {
    private Long id;
    private String name;
    private byte scope;
    private byte status;
    private Date dateCreated;
    private Date dateModified;
    private String shortcut;
    private String description;
    private String icon;
    private long group;
    private String ftpAccount;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public byte getScope() {
        return this.scope;
    }

    public void setScope(byte scope) {
        this.scope = scope;
    }


    public byte getStatus() {
        return this.status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }


    public Date getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }


    public Date getDateModified() {
        return this.dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }


    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getShortcut() {
        return this.shortcut;
    }

    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }


    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public long getGroup() {
        return this.group;
    }

    public void setGroup(long group) {
        this.group = group;
    }

    public String getFtpAccount() { return this.ftpAccount; }

    public void setFtpAccount(String ftpAccount) { this.ftpAccount = ftpAccount; }

}

