package com.ss8.metahub.shared.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/***
/// TODO: recode using java.util.Base64.De/Encoder in Java 1.8
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
***/

@Deprecated
public class PasswordEncrypter {
	
	 private static final char[] PASSWORD = "security-analytics-ss8".toCharArray();
    private static final byte[] SALT = {
        (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,
        (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,
    };
    static String CIPHER_METHOD = "PBEWithMD5AndDES";
    static final int SALT_CYCLES = 20;  // should be much higher
    
    static Cipher initCipher(int cipherMode) throws GeneralSecurityException {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(CIPHER_METHOD);
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
        Cipher pbeCipher = Cipher.getInstance(CIPHER_METHOD);
        pbeCipher.init(cipherMode, key, new PBEParameterSpec(SALT, SALT_CYCLES));
        return (pbeCipher);
    }
    
    public static String encrypt(String property) throws GeneralSecurityException, UnsupportedEncodingException {
        Cipher pbeCipher = initCipher(Cipher.ENCRYPT_MODE);
        return (base64Encode(pbeCipher.doFinal(property.getBytes("UTF-8"))));
    }
    public static String encrypt(byte[ ] clearData) throws GeneralSecurityException, UnsupportedEncodingException {
    	return (encrypt(new String(clearData, "UTF-8")));
    }

    private static String base64Encode(byte[] bytes) {
    	return (Base64.getEncoder( ).encodeToString(bytes));
     	// return new BASE64Encoder().encode(bytes);
    }

    public static String decrypt(String property)  {
    	try {
	        Cipher pbeCipher = initCipher(Cipher.DECRYPT_MODE);
	        return (new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8"));
    	} catch (Exception ex) {
    		return (property);
    	}
    }
    public static String decrypt(byte[ ] encryptedData) throws GeneralSecurityException, UnsupportedEncodingException  {
    	return (decrypt(new String(encryptedData, "UTF-8")));
    }

    private static byte[] base64Decode(String property) throws IOException {
    	return (Base64.getDecoder( ).decode(property));
       	// return new BASE64Decoder().decodeBuffer(property);
    }
    
   
	public static void main(String[] args) throws Exception {
    	 String originalPassword = "P@ssw0rd*";
    	  if(args == null || args.length == 0 )
    		  originalPassword = "ss8";
    	  else
    		  originalPassword = args[0];
       
        String encryptedPassword = encrypt(originalPassword);
        String decryptedPassword = decrypt("NZUm21nG6T0=");
        System.out.println("password Entered: " + originalPassword +" -encrypt-"+encryptedPassword +"-decrpty-"+decryptedPassword);
       
    }
}
