package com.ss8.metahub.shared.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "eventTypes", "condition" })
public class Query {

	@JsonProperty("eventTypes")
	private List<String> eventTypes = null;
	@JsonProperty("condition")
	private Condition condition;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("eventTypes")
	public List<String> getEventTypes() {
		return eventTypes;
	}

	@JsonProperty("eventTypes")
	public void setEventTypes(List<String> eventTypes) {
		this.eventTypes = eventTypes;
	}

	@JsonProperty("condition")
	public Condition getCondition() {
		return condition;
	}

	@JsonProperty("condition")
	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
