package com.ss8.metahub.shared.util;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.List;
import org.testng.annotations.Test;

import com.ss8.metahub.shared.dto.GeoPoint;

public class PolygonCoordinatesTest {
	
	@Test
	public void createPolygonTest(){
		List<GeoPoint> lonLatList = PolygonCoordinates.createPolygon(12.972442,77.580643,38,120,0.1);
		assertNotNull(lonLatList);
		//Get first LonLat
		GeoPoint geoPoint1 = lonLatList.get(0);
		assertEquals(12.972442, geoPoint1.getLongitude(), 0);
		assertEquals(77.580643, geoPoint1.getLatitude(), 0);
		//Python library and Java library can show a delta sometimes < than 0.0000000000001
		//Capturing the delta test below
		GeoPoint geoPoint2 = lonLatList.get(1);
		assertEquals(12.970877177447388, geoPoint2.getLongitude(), 0);
		//Just using delta as 0.0001 because the python generates 77.58147590377168, Java generates 77.58147590377169
		assertEquals(77.58147590377168, geoPoint2.getLatitude(), 0.0001);
	}

}
